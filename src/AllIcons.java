import javax.swing.*;
public class AllIcons{
	/*Log in*/
	protected static ImageIcon LogInbackGround = new ImageIcon("../image/LoginBG.jpg");
	/*Main Screen*/
	protected static ImageIcon night = new ImageIcon("../image/night.png");
	protected static ImageIcon mapTag = new ImageIcon("../image/LoserVillageTag.png");
	protected static ImageIcon LoserVillageTag = new ImageIcon("../image/LoserVillageTag.png");
	protected static ImageIcon block = new ImageIcon("../image/block.png");
	
	/*ESCMenu*/
	protected static ImageIcon pointerIcon = new ImageIcon("../image/Pointer.PNG");
	protected static ImageIcon ESCbackGround = new ImageIcon("../image/ESCBG.png");
	/*NPC*/
	protected static ImageIcon dialogIcon = new ImageIcon("../image/dialog.jpg");
	/*Tiny Map*/
	protected static ImageIcon TinyMC = new ImageIcon("../image/TinyMC.JPG");
	
	/*Fighting Screen*/
	protected static ImageIcon ProgBar = new ImageIcon("../image/fighting/bar.png");
	protected static ImageIcon FightBG = new ImageIcon("../image/fighting/fighting_Screen.jpg");
	protected static ImageIcon FightBG2 = new ImageIcon("../image/fighting/grass1.jpg");
	
	/*Attack menu*/
	protected static ImageIcon menu0 = new ImageIcon("../image/fighting/attack_menu0.png");
	protected static ImageIcon menu1 = new ImageIcon("../image/fighting/attack_menu1.png");
	protected static ImageIcon menu2 = new ImageIcon("../image/fighting/attack_menu2.png");
	protected static ImageIcon menu3 = new ImageIcon("../image/fighting/attack_menu3.png");
	protected static ImageIcon menu4 = new ImageIcon("../image/fighting/attack_menu4.png");
	protected static ImageIcon menu5 = new ImageIcon("../image/fighting/attack_menu5.png");
	
	protected static ImageIcon Arrow = new ImageIcon("../image/fighting/arrow.png");
	
	/*Monsters*/
	protected static ImageIcon Monst0 = new ImageIcon("../image/fighting/slime.png");
	protected static ImageIcon Monst1 = new ImageIcon("../image/fighting/pikaman.png");
	
	/*Bag*/
	protected static ImageIcon BagBG = new ImageIcon("../image/bag/BagBG.png");
	protected static ImageIcon ItemBG = new ImageIcon("../image/bag/ItemBG.png");
	protected static ImageIcon WeaponBG = new ImageIcon("../image/bag/WeaponBG.png");
	protected static ImageIcon choose_box = new ImageIcon("../image/bag/choose_box.png");
	protected static ImageIcon choose_box2 = new ImageIcon("../image/bag/choose_box2.png");
	
	
	/*item icon*/
	protected static ImageIcon red_water_icon = new ImageIcon("../image/bag/items/red_water.png");
	protected static ImageIcon blue_water_icon = new ImageIcon("../image/bag/items/blue_water.png");
	protected static ImageIcon yellow_water_icon = new ImageIcon("../image/bag/items/yellow_water.png");
	protected static ImageIcon green_water_icon = new ImageIcon("../image/bag/items/green_water.png");
	protected static ImageIcon apple_icon = new ImageIcon("../image/bag/items/apple.png");
	protected static ImageIcon mushroom_icon = new ImageIcon("../image/bag/items/mushroom_water.png");
	protected static ImageIcon bread_icon = new ImageIcon("../image/bag/items/bread.png");
	
	/*weapon icon*/
	protected static ImageIcon long_sword1_icon = new ImageIcon("../image/bag/weapons/long_sword1.png");
	
	
	
}