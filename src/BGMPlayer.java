import java.io.*; 
import java.lang.*;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.*;
public class BGMPlayer{
	protected static Clip BGMclip;
	protected static Clip BGMBattleclip;
	public static void playBGM(String BGMName) throws Throwable {
        BGMclip = AudioSystem.getClip();
		File file = new File("../music/"+BGMName);
        AudioInputStream inputStream = AudioSystem.getAudioInputStream(file);
        BGMclip.open(inputStream);
        BGMclip.loop(Clip.LOOP_CONTINUOUSLY); 
    }
	public static void playBattleBGM() throws Throwable {
        BGMBattleclip = AudioSystem.getClip();
		File filebattle = new File("../music/_n28.wav");
        AudioInputStream BattleinputStream = AudioSystem.getAudioInputStream(filebattle);
        BGMBattleclip.open(BattleinputStream);
        BGMBattleclip.loop(Clip.LOOP_CONTINUOUSLY); 
    }
}