/*Bag 裡面目前規劃有三大區塊
1.Weapon 武器類(主要是增加攻擊 可視情況有其他bonus例如exp*1.2之類 或 AGI降低) ex.刀 槍 劍 ...
2.Equip 裝備類(主要是增加防禦) ex. 頭盔 鎧甲 手套...
3.Item 道具類 ex.補品 短時間提升攻擊防禦用的水...
還是要把2合併到1 分成兩大種就好?

Bag panel size = 1000x600, label的位置為panel上的絕對位置
ItemBG、WeaponBG size = 1000x600, 實際上有東西的背景為450*600

###結論:只分成兩塊(裝備&道具)###

method:
public void show_bag() 生出一個panel然後展示物品 可以在上面移來移去選擇使用之類的
public boolean add_item(Item _item, int num) 放入item到背包裡 _item是要增加的item num是數量 成功回傳true 背包已滿回傳false
public boolean use_item(Item _item) 使用_item 成功回傳true 失敗(背包裡沒有)回傳false
public boolean throw_item(Item _item) 丟掉_item 成功回傳true 失敗(背包裡沒有)回傳false
public boolean add_weapon(Weapon _weapon)
public boolean throw_weapon(Weapon _weapon)

equip weapon流程: 在weapon上按空白鍵->call Bag的equip weapon() -> call MC equip -> call weapon equip

*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
public class Bag{
	int max_size = 16;
	public static boolean selected;
	public static boolean ReturnFromESC;
	public static boolean CallFromFighting = false;
	private LinkedList<Weapon> weapon;
	private	LinkedList<Item> item;
	private int[] item_num;
	BagKeyHandler baghandler;
	ItemKeyHandler itemhandler;
	WeaponKeyHandler weaponhandler;
	KeyAdapter mainScreenHandler;
	JFrame frame = new JFrame() ;
	
	JPanel bag_panel = new JPanel();
	JPanel item_panel = new JPanel();
	JPanel weapon_panel = new JPanel();
	
	JLabel BagBG_label = new JLabel();
	JLabel choose_box1 = new JLabel();
	JLabel choose_box2 = new JLabel();
	
	public Bag(LosersAndWinner _frame , KeyAdapter _mainScreenHandler) {
		weapon = new LinkedList<Weapon>();
		item = new LinkedList<Item>();
		item_num = new int[max_size];
		for(int i=0;i<max_size;i++) {
			item_num[i] = 0;
		}
		///test
		frame = _frame;
		mainScreenHandler=_mainScreenHandler;
	/*	frame.setSize(1280,800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setVisible(true);*/
		///test
	}
	public void show_bag() {
		selected = false;
		ReturnFromESC = false;
		//bag_panel.setBackground(Color.black);
		frame.removeKeyListener(mainScreenHandler);
		bag_panel.setOpaque(false);
		bag_panel.setLayout(null);
		bag_panel.setSize(1000,600);//(screenWidth,screenHeight);
		
		BagBG_label.setIcon(AllIcons.BagBG);
		BagBG_label.setOpaque(false);
		//BagBG_label.setIcon(BagBG);
		BagBG_label.setBounds(0,0,1000,600);//screenWidth,screenHeight);
		bag_panel.add(BagBG_label,0);
		JLabel num_label = new JLabel();
		JLabel equip_label = new JLabel("E");
		if(weapon.size()!=0) {
			JLabel[] weapon_label = new JLabel[weapon.size()];
			for(int i=0;i<weapon.size();i++) {
				weapon_label[i] = new JLabel();
				weapon_label[i].setIcon(weapon.get(i).get_icon());
				weapon_label[i].setBounds(90+110*i,60+((i)/8)*110,100,100);
				bag_panel.add(weapon_label[i],0);
				
				if(weapon.get(i).get_equiped()) {
					equip_label.setBounds(weapon_label[i].getX()+65,weapon_label[i].getY()+65,35,35);
					equip_label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 30));
					equip_label.setForeground(Color.WHITE);
					equip_label.setBackground(Color.BLACK);
					equip_label.setOpaque(true);
					equip_label.setHorizontalAlignment(JLabel.CENTER);
					equip_label.setVerticalAlignment(JLabel.CENTER);
					bag_panel.add(equip_label,0);
				}
			}
		}
		if(item.size()!=0) {
			JLabel[] item_label = new JLabel[item.size()];
			for(int i=0;i<item.size();i++) {
				item_label[i] = new JLabel();
				item_label[i].setIcon(item.get(i).get_icon());
				item_label[i].setBounds(90+110*i,310+((i)/8)*110,100,100);
				bag_panel.add(item_label[i],0);
				
				//num_label = new JLabel("<html>hello <br> world!</html>");
				num_label = new JLabel(item_num[i]+"");
				num_label.setBounds(item_label[i].getX()+65,item_label[i].getY()+65,35,35);
				num_label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 25));
				num_label.setForeground(Color.WHITE);
				num_label.setBackground(Color.BLACK);
				num_label.setOpaque(true);
				num_label.setHorizontalAlignment(JLabel.CENTER);
				num_label.setVerticalAlignment(JLabel.CENTER);
				bag_panel.add(num_label,0);
				
			}
		}
		choose_box1.setIcon(AllIcons.choose_box);
		//choose_box.setIcon(choose_box);
		choose_box1.setBounds(90,60,100,100);
		bag_panel.add(choose_box1,0);
		
		baghandler = new BagKeyHandler();
		frame.addKeyListener(baghandler);
		bag_panel.setVisible(true);
		
		///test
		frame.add(bag_panel,0);
		bag_panel.setBounds((LosersAndWinner.screenWidth-1000)/2,(LosersAndWinner.screenHeight-600)/2,1000,600);
		frame.repaint();
		///test
	}
	protected void backToMainScreen(){
		frame.removeKeyListener(baghandler);
		if(!CallFromFighting)
			frame.addKeyListener(mainScreenHandler);
		
		frame.remove(bag_panel);
		frame.repaint();
	}
	class BagKeyHandler extends KeyAdapter{
		public void keyPressed(KeyEvent e) {
			int x = choose_box1.getX();
			int y = choose_box1.getY();
			if(e.getKeyCode() == e.VK_UP){
				if(y==170 || y==420) {
					choose_box1.setBounds(x,y-110,100,100);
				}
				else if(y==90) {
					choose_box1.setBounds(x,420,100,100);
				}
				else if(y==310) {
					choose_box1.setBounds(x,170,100,100);
				}
			}
			else if(e.getKeyCode() == e.VK_DOWN){
				if(y==60 || y==310) {
					choose_box1.setBounds(x,y+110,100,100);
				}
				else if(y==170) {
					choose_box1.setBounds(x,310,100,100);
				}
				else if(y==420) {
					choose_box1.setBounds(x,60,100,100);
				}
			}
			else if(e.getKeyCode() == e.VK_LEFT){
				if(x==90) {
					choose_box1.setBounds(860,y,100,100);
				}
				else {
					choose_box1.setBounds(x-110,y,100,100);
				}
			}
			else if(e.getKeyCode() == e.VK_RIGHT){
				if(x==860) {
					choose_box1.setBounds(90,y,100,100);
				}
				else {
					choose_box1.setBounds(x+110,y,100,100);
				}
			}
			else if(e.getKeyCode() == e.VK_SPACE) {
				if(y==310) {
					if(item.size()>((x-90)/110)) {
						show_item(item.get((x-90)/110));
					}
				}
				else if(y==420) {
					if(item.size()>(((x-90)/110)+8)) {
						show_item(item.get((x/110)+8));
					}
				}
				else if(y==60) {
					if(weapon.size()>((x-90)/110)) {
						show_weapon(weapon.get((x-90)/110));
					}
				}
				else if(y==170) {
					if(weapon.size()>(((x-90)/110)+8)) {
						show_weapon(weapon.get(((x-90)/110)+8));
					}
				}
			}
			else if(e.getKeyCode() == e.VK_ESCAPE || e.getKeyCode() == e.VK_B) {
				ReturnFromESC = true;
				backToMainScreen();
			}
		}
	}
	public void show_item(Item item) {
	
		frame.removeKeyListener(baghandler);
		item_panel.setLayout(null);
		item_panel.setSize(1000,600);
		item_panel.setVisible(true);
		item_panel.setOpaque(false);
		item_panel.removeAll();
		
		JLabel BG = new JLabel();
		BG.setIcon(AllIcons.ItemBG);
		BG.setBounds(0,0,1000,600);
		item_panel.add(BG,0);

		JLabel name_label = new JLabel(item.get_name());
		name_label.setBounds(470,100,200,50);
		name_label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
		item_panel.add(name_label,0);
		
		JLabel description_label = new JLabel(item.get_description());
		description_label.setBounds(470,200,200,100);
		description_label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		item_panel.add(description_label,0);
		
		JLabel icon_label = new JLabel();
		icon_label.setIcon(item.get_icon());
		icon_label.setBounds(350,100,100,100);
		item_panel.add(icon_label,0);
		
		choose_box2.setIcon(AllIcons.choose_box2);
		choose_box2.setBounds(350,300,100,50);
		item_panel.add(choose_box2,0);
		
		itemhandler = new ItemKeyHandler(item);
		frame.addKeyListener(itemhandler);
		
		frame.add(item_panel,0);
		item_panel.setBounds((LosersAndWinner.screenWidth-1000)/2,(LosersAndWinner.screenHeight-600)/2,1000,600);
		frame.repaint();
		//item_panel.setFocusable(true);
		
	
	}
	class ItemKeyHandler extends KeyAdapter{
		Item item;
		public ItemKeyHandler(Item _item) {
			item = _item;
		}
		public void keyPressed(KeyEvent e) {
			int x = choose_box2.getX();
			int y = choose_box2.getY();
			if(e.getKeyCode() == e.VK_UP || e.getKeyCode() == e.VK_DOWN){
				if(y==300) {
					choose_box2.setBounds(350,400,100,50);
				}
				else if(y==400) {
					choose_box2.setBounds(350,300,100,50);
				}
			}
			else if(e.getKeyCode() == e.VK_SPACE) {
				//使用
				if(y==300) {
					frame.remove(item_panel);
					frame.removeKeyListener(itemhandler);
					
					//for fighting system
					selected = true;						
				}
				//丟棄
				else if(y==400) {
					throw_item(item);
					frame.remove(item_panel);
					frame.removeKeyListener(itemhandler);
					show_bag();
				}
			}
			else if(e.getKeyCode() == e.VK_ESCAPE) {
				frame.remove(item_panel);
				frame.removeKeyListener(itemhandler);
				//frame.addKeyListener(baghandler);
				show_bag();
			}
		}
	}
	public void show_weapon(Weapon weapon) {
	
		frame.removeKeyListener(baghandler);
		weapon_panel.setLayout(null);
		weapon_panel.setSize(1000,600);
		weapon_panel.setVisible(true);
		weapon_panel.setOpaque(false);
		weapon_panel.removeAll();
		
		JLabel BG = new JLabel();
		BG.setIcon(AllIcons.WeaponBG);
		//BG.setIcon(WeaponBG);
		BG.setBounds(0,0,1000,600);
		weapon_panel.add(BG,0);

		JLabel name_label = new JLabel(weapon.get_name());
		name_label.setBounds(470,100,200,50);
		name_label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
		weapon_panel.add(name_label,0);
		
		JLabel description_label = new JLabel(weapon.get_description());
		description_label.setBounds(470,200,200,100);
		description_label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
		weapon_panel.add(description_label,0);
		
		if(weapon.get_equiped()) {
			JLabel equip_label = new JLabel("裝備者 : "+weapon.get_equiper());
			equip_label.setBounds(470,300,200,50);
			equip_label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
			weapon_panel.add(equip_label,0);
		}
		
		JLabel icon_label = new JLabel();
		icon_label.setIcon(weapon.get_icon());
		icon_label.setBounds(350,100,100,100);
		weapon_panel.add(icon_label,0);
		
		choose_box2.setIcon(AllIcons.choose_box2);
		choose_box2.setBounds(350,250,100,50);
		weapon_panel.add(choose_box2,0);
		
		weaponhandler = new WeaponKeyHandler(weapon);
		frame.addKeyListener(weaponhandler);
		
		frame.add(weapon_panel,0);
		weapon_panel.setBounds((LosersAndWinner.screenWidth-1000)/2,(LosersAndWinner.screenHeight-600)/2,1000,600);
		
		frame.repaint();
		frame.revalidate();
		//item_panel.setFocusable(true);
		
	
	}
	class WeaponKeyHandler extends KeyAdapter{
		Weapon weapon;
		public WeaponKeyHandler(Weapon _weapon) {
			weapon = _weapon;
		}
		public void keyPressed(KeyEvent e) {
			int x = choose_box2.getX();
			int y = choose_box2.getY();
			if(e.getKeyCode() == e.VK_UP) {
				if(y==250) {
					choose_box2.setBounds(350,410,100,50);
				}
				else {
					choose_box2.setBounds(350,y-80,100,50);
				}
			}
			if(e.getKeyCode() == e.VK_DOWN){
				if(y==410) {
					choose_box2.setBounds(350,250,100,50);
				}
				else {
					choose_box2.setBounds(350,y+80,100,50);
				}
			}
			else if(e.getKeyCode() == e.VK_SPACE) {
				//裝備
				if(y==250) {
					
				}
				//解除裝備
				else if(y==330) {
				}
				//丟棄
				else if(y==410) {
					throw_weapon(weapon);
					frame.remove(weapon_panel);
					frame.removeKeyListener(weaponhandler);
					show_bag();
				}
			}
			else if(e.getKeyCode() == e.VK_ESCAPE) {
				frame.remove(weapon_panel);
				frame.removeKeyListener(weaponhandler);
				//frame.addKeyListener(baghandler);
				show_bag();
			}
		}
	}
						
	public boolean add_item(Item _item, int num) {
		int i=0;
		while (i<item.size()){
			if(item.get(i).get_name().equals(_item.get_name())) {
				item_num[i] = item_num[i]+num;
				return true;
			}
			i++;
		}
		if(i<max_size) {
			item.add(_item);
			item_num[i] = num;
			return true;
		}
		else if(i>=max_size) {
			return false;
		}
		return false;
	}
	public boolean use_item(Item _item) {
		for(int i=0;i<item.size();i++) {
			if(item.get(i).get_name().equals(_item.get_name())) {
				item_num[i]--;
				if(item_num[i]==0) {
					for(int j=i;j<item.size()-1;j++) {
						item_num[j] = item_num[j+1];
					}
					item_num[item.size()-1] = 0;
					item.remove(i);
				}
				return true;
			}
		}
		return false;
	}
	public boolean throw_item(Item _item) {
		for(int i=0;i<item.size();i++) {
			if(item.get(i).get_name().equals(_item.get_name())) {
					for(int j=i;j<item.size()-1;j++) {
						item_num[j] = item_num[j+1];
					}
					item_num[item.size()-1] = 0;
					item.remove(i);
			return true;
			}
		}
		return false;
	}
	public boolean add_weapon(Weapon _weapon) {
		if(weapon.size()>=max_size) {
			return false;
		}
		else {
			weapon.add(_weapon);
			return true;
		}
	}
	public boolean throw_weapon(Weapon _weapon) {
		return weapon.remove(_weapon);
	}
}