/**
暫停畫面
Method:
CallESCMenu(): 在主畫面按下ESC後，會呼叫此method並轉移控制權
Back() : 回到主畫面，並還回控制權
*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class ESCMenu extends JLabel{
	private ESCMenuHandler escHandler= new ESCMenuHandler(this);
	private JLabel[] menuLabel = new JLabel[5];
	private JLabel pointer = new JLabel();
	
	private static MainScreen mainScreen;
	protected int count;
	protected LosersAndWinner LAW;
	public ESCMenu(MainScreen ms , LosersAndWinner jf){
		mainScreen = ms;
		LAW = jf;
		AllIcons.ESCbackGround.setImage(AllIcons.ESCbackGround.getImage().getScaledInstance(mainScreen.screenWidth, mainScreen.screenHeight, Image.SCALE_DEFAULT));
		this.setIcon(AllIcons.ESCbackGround);
		menuLabel[0] = new JLabel("Continue");
		//menuLabel[0].setText("繼續遊戲");
		menuLabel[1] = new JLabel("Save");
		menuLabel[2] = new JLabel("Load");
		menuLabel[3] = new JLabel("Option");
		menuLabel[4] = new JLabel("Quit");
		pointer.setIcon(AllIcons.pointerIcon);
		for(int i=0 ; i<5 ; i++){
			menuLabel[i].setFont(new Font("Comic Sans MS", Font.PLAIN, 50));
			menuLabel[i].setForeground(Color.white);
		}
	}
	public void CallESCMenu(){
		LAW.removeKeyListener(mainScreen.handler);
		LAW.addKeyListener(escHandler);
		LAW.add(this,0);
		this.setBounds(0,0,mainScreen.screenWidth,mainScreen.screenHeight);
		for(int i=0 ; i<5 ; i++){
			LAW.add(menuLabel[i],0);
			menuLabel[i].setBounds(70,30+60*i,mainScreen.screenWidth,50);
		}
		LAW.add(pointer,0);
		pointer.setBounds(10,30,50,50);
		LAW.repaint();
	}
	public void Back(){
		LAW.removeKeyListener(escHandler);
		LAW.remove(this);
		for(int i=0 ; i<5 ; i++)
			LAW.remove(menuLabel[i]);
		LAW.remove(pointer);
		LAW.repaint();
		LAW.addKeyListener(mainScreen.handler);
	}
	public void MovePointer(){
		pointer.setBounds(10,30+(60*count),50,50);
	}
}
class ESCMenuHandler extends KeyAdapter{
	private ESCMenu esc;
	public ESCMenuHandler(ESCMenu e){
		esc = e;
	}
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == e.VK_ESCAPE){
			esc.Back();
		}
		if(e.getKeyCode() == e.VK_UP){
			if(esc.count>0){
				esc.count--;	
				esc.MovePointer();
			}
		}
		if(e.getKeyCode() == e.VK_DOWN){
			if(esc.count<4){
				esc.count++;
				esc.MovePointer();
			}
		}
		if(e.getKeyCode() == e.VK_SPACE || e.getKeyCode() == e.VK_ENTER){
			if(esc.count==0)     //Continue Game
				esc.Back();
			else if(esc.count==1);//Save
			else if(esc.count==2);//Load
			else if(esc.count==3);//Option
			else if(esc.count==4);//Quit
			
			
		}
	}
}