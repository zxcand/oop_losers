import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class MapEditor {
	public static void main() {
		int MAPSIZE_X;
		int MAPSIZE_Y;
		int pos_x;
		int pos_y;
		int end = 0;
		
		int object;
		
		JLabel icon;

		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Setting map default environment:");
		int environment = scanner.nextInt();

		for(int i=0;i<MAXSIZE_Y;i++) {
			for(int j=0;j<MAXSIZE_X;j++) {
				map[i][j] = 2;
				icon = new JLabel();
				icon.setIcon(Background[environment]);
				icon.setBounds(i*50,j*50,50,50);
				icon.add(map_panel);
			}
		}

		
		System.out.println("Setting map size:");
		System.out.print("x = ");
		MAPSIZE_X = scanner.nextInt();
		System.out.print("y = ");
		MAPSIZE_Y = scanner.nextInt();
		int[][] map = new int[MAPSIZE_Y][MAPSIZE_X];
		JPanel map_panel = new JPanel();
		map_panel.setLayout = (null);
		map_panel = setSize(MAPSIZE_X*50,MAPSIZE_Y*50);
		
		
		while(end==0) {
			System.out.print("what do you want to add?\n");
			System.out.print("exit=-1000 \n");
			System.out.print("Show current status = -500\n");
			object = scanner.nextInt();
			if(object==-1000) break;
			if(object==-500) {
				JFrame frame = new JFrame();
				frame.setSize(MAPSIZE_X*50,MAPSIZE_Y*50);
				frame.add(map_panel);
				frame.setVisible(true);
				for(int i=0;i<MAPSIZE_Y;i++) {
					for(int j=0;j<MAPSIZE_X;j++) {
						System.out.print(map[i][j]+"");
					}
					System.out.print("\n");
				}
				continue;
			}
			System.out.print("Set position(if > 1, Left-Up):\n");
			System.out.print("x = ");
			pos_x = scanner.nextInt();
			System.out.print("y = ");
			pos_y = scanner.nextInt();
			
			switch(object) {
				case 1: { //tree
					map[pos_y][pos_x] = 1;
					icon = new JLabel();
					icon.setIcon(Decoration[0]);
					icon.setBounds(pos_x*50,pos_y*50,50,50);
					icon.add(map_panel);
					break;
				}
				case 2: { //grass
					map[pos_y][pos_x] = 2;
					icon.setIcon(Background[0]);
					icon.setBounds(pos_x*50,pos_y*50,50,50);
					icon.add(map_panel);
					break;
				}
				case 3: { //grass flower
					map[pos_y][pos_x] = 3;
					icon.setIcon(Background[1]);
					icon.setBounds(pos_x*50,pos_y*50,50,50);
					icon.add(map_panel);
				}
				case 4: { //stone
					map[pos_y][pos_x] = 4;
					icon.setIcon(Decoration[1]);
					icon.setBounds(pos_x*50,pos_y*50,50,50);
					icon.add(map_panel);
				}
				case 5: { //hole
					map[pos_y][pos_x] = 5;
					icon.setIcon(wall);
					icon.setBounds(pos_x*50,pos_y*50,50,50);
					icon.add(map_panel);
				}
				case 6: { //house
					for(int i=0;i<3;i++) {
						for(int j=0;j<3;j++) {
							map[pos_y+i][pos_x+j] = 6;
						}
					}
					icon.setIcon(House[0]);
					icon.setBounds(pos_x*50,pos_y*50,150,150);
					icon.add(map_panel);
					break;
				}
				case 7: { //
				}
				case 8: {
				}
				case 9: {
				}
			}
		}
	}
}
						
			
			
			
		

		