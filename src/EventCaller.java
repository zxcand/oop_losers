import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
public class EventCaller{
	private static String eventMessage;
	private static JLabel messageLabel = new JLabel();
	private static EventHandler eventhandler = new EventHandler();
	private static MainScreen mainScreen;
	private static JLabel Dialog = new JLabel(AllIcons.dialogIcon);
	private static JLabel MCNameLabel = new JLabel();
	public static void Event(int eventID , MainScreen _mainScreen){
		mainScreen=_mainScreen;
		eventMessage = XMLParser.EventMessage(Integer.toString(eventID));
		LosersAndWinner.mainFrame.removeKeyListener(mainScreen.handler);
		mainScreen.EventWalk();
		
		/*Dialog Frame*/
		LosersAndWinner.mainFrame.add(Dialog,0);
		Dialog.setBounds(0,mainScreen.screenHeight-200,mainScreen.screenWidth,200);
		/*MCName Label*/
		LosersAndWinner.mainFrame.add(MCNameLabel,0);
		Font font=new Font(Font.DIALOG_INPUT,Font.ITALIC, 30);
		MCNameLabel.setFont(font);
		MCNameLabel.setText("�Z");
		MCNameLabel.setBounds(0,mainScreen.screenHeight-175,mainScreen.screenWidth,30);
		MCNameLabel.setForeground(Color.red);
		/*Message Label*/
		messageLabel.setText(eventMessage);
		font=new Font(Font.DIALOG_INPUT,Font.BOLD, 30);
		messageLabel.setFont(font);
		messageLabel.setForeground(Color.white);
		LosersAndWinner.mainFrame.add(messageLabel,0);
		messageLabel.setBounds(0,mainScreen.screenHeight-135,mainScreen.screenWidth,135);
		LosersAndWinner.mainFrame.repaint();
		LosersAndWinner.mainFrame.addKeyListener(eventhandler);
	}
	public static void end(){
		LosersAndWinner.mainFrame.remove(Dialog);
		LosersAndWinner.mainFrame.remove(MCNameLabel);
		LosersAndWinner.mainFrame.remove(messageLabel);
		LosersAndWinner.mainFrame.removeKeyListener(eventhandler);
		LosersAndWinner.mainFrame.addKeyListener(mainScreen.handler);
		LosersAndWinner.mainFrame.repaint();
	}
	static class EventHandler extends KeyAdapter{
		/*EventCaller event;
		public EventHandler(EventCaller _event){
			event = _event;
		}*/
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == e.VK_SPACE)
				EventCaller.end();
		}
	}
}