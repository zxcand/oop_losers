import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class FightingScreen extends JPanel
{	
	public static JFrame mainJF;

	private static final int[][] mc_pos = {{LosersAndWinner.screenWidth-400,LosersAndWinner.screenHeight-500},{LosersAndWinner.screenWidth-500,LosersAndWinner.screenHeight-400},{LosersAndWinner.screenWidth-600,LosersAndWinner.screenHeight-300}};
	private static final int[][] monst_pos = {{125,325},{225,225},{325,125}};

	private JLabel BGLabel;
	
	private JLabel[] MCLabel = new JLabel[3];
	
	protected static LinkedList<Monster> MonsterList;
	
	public  int Monst_Num;
	public  int Alive_Monst_Num;
	private Fighting_ProgBar pb;
	private KeyAdapter mapHandler;
	private static int Monst_level;	
	private static JLabel mc_arrow  = new JLabel();
	
	static{
		mc_arrow.setIcon(AllIcons.Arrow);	
	}
	
	//paras for progressbar
	public static final int bar_pos_x = LosersAndWinner.screenWidth-530;
	public static final int bar_pos_y = 60;
	public static final int bar_width  = 500;
	public static final int bar_height = 100;
	
	public FightingScreen(JFrame jf, KeyAdapter _mapHandler)
	{
		this(jf, 1);//default to 1
		mapHandler=_mapHandler;
		Fighting_ProgBar.RESUME();	
	}
	
	public FightingScreen(JFrame jf, int level)//int situation
	{		
		setOpaque(true);
		setLayout(null);
		
		mainJF = jf;
		Monst_level = level;
		Monst_Num = 2*level;//[TODO] some random assignment
		setBounds(0, 0, LosersAndWinner.screenWidth, LosersAndWinner.screenHeight);
		
		AddBG();
		AddMC();
		AddMonst();
		AddProgBar();		
	}
	
	private void AddBG()
	{
		Image ii;
		if(Monst_level == 1)
			 ii= AllIcons.FightBG.getImage().getScaledInstance(LosersAndWinner.screenWidth, LosersAndWinner.screenHeight, Image.SCALE_SMOOTH);
		else
			ii = AllIcons.FightBG2.getImage().getScaledInstance(LosersAndWinner.screenWidth, LosersAndWinner.screenHeight, Image.SCALE_SMOOTH);
		
		BGLabel = new JLabel(new ImageIcon(ii));
		this.add(BGLabel,0);
		BGLabel.setBounds(0,0, LosersAndWinner.screenWidth, LosersAndWinner.screenHeight);
	}
	
	private void AddMC()
	{
		//Seting labels
		for(int i=0; i<3; i++)
		{
			MCLabel[i] = new JLabel();
			MCLabel[i].setIcon(MainScreen.MCList[i].getFightingIcon());
			
			MCLabel[i].setBounds(mc_pos[i][0],mc_pos[i][1],100,100);
			this.add(MCLabel[i],0);
		}
	}
	
	//[TODO]
	private Monster RandomMonstType(int level)
	{
		int idx;
		Random rand = new Random();
		idx = rand.nextInt(2);//0 or 1
		return Monster.GetMonster(idx);
	}
	
	private void AddMonst()
	{
		MonsterList = new LinkedList<Monster>();
		
		for(int i=0; i<Monst_Num; i++)
		{
			Monster temp = RandomMonstType(Monst_level);
			temp.MonsterLabel.setBounds(monst_pos[i][0],monst_pos[i][1],100,100);
			this.add(temp.MonsterLabel,0);
			MonsterList.add(temp);
			
			
			temp.BloodIcon.setBounds(monst_pos[i][0],monst_pos[i][1]+100,100,20);
			this.add(temp.BloodIcon,0);
		}
	}
		
	private void AddProgBar()
	{
		pb = new Fighting_ProgBar(this);
		pb.setBounds(bar_pos_x,bar_pos_y,bar_width,bar_height);
		this.add(pb,0);
		
		Thread t = new Thread(pb);
		t.start();
	}
	
	public int RandomDamage(int d)
	{
		//0.8~1.2X
		Random rand = new Random();
		int range  = d/5;
		int damage = 4*range + rand.nextInt(2*range);
		return damage;
	}
	
	public void MC_Attack(int attacker, int target)
	{
		int hurt = RandomDamage(MainScreen.MCList[attacker].getATT()); 
		System.out.println("hurts " + hurt +" points on target "+target);	
		MonsterList.get(target).setHP( MonsterList.get(target).getHP() - hurt);
	}
	
	public void MC_Attack(int attacker, int target, int skill)
	{
	
	
	}
	
	public void MC_Turn(int whose_turn)//(MC now)
	{
		//showing the menu of skill that can be appllied
		//MC mc = now;

		//Skill sk
		ActionAndTarget info = new ActionAndTarget(0,-1);
		
		JLabel menu = new AttackMenu(info);
		menu.setBounds(LosersAndWinner.screenWidth-350,LosersAndWinner.screenHeight-350,250,250);
		add(menu,0);
		repaint();
		
		//(1) Choosing skill(assign value to var. sk)
		ChoosingAction(info, whose_turn, menu);
		
		//(2)Quit or Choosing the target		
		if(info.action==5)
		{
			Fighting_ProgBar.END();
			mainJF.remove(this);
			mainJF.repaint();
			mainJF.addKeyListener(mapHandler);
			BGMPlayer.BGMBattleclip.stop();
		}
		else if(info.action==1)
		{
			ChoosingTarget(info);
			MC_Attack(whose_turn,info.target);
		}
		else if(info.action == 2)//skill
		{
			//ChoosingSkill();
			ChoosingTarget(info);
		}
	}
	
	
	public void ChoosingAction(ActionAndTarget info, int whose_turn, JLabel menu)
	{
		int action;
		while(true)
		{
			action = info.action;
			mc_arrow.setBounds(mc_pos[whose_turn][0]-15+50, mc_pos[whose_turn][1]-50, 30, 50);
			add(mc_arrow,0);
			repaint();	
			
			if(action == 0)
			{
				try{
					Thread.sleep(500);
				}
				catch(InterruptedException ie){}
				
				continue;
			}
			else if(action == 4)//��
			{
				System.out.println("Using Item");
				MainScreen.bag.CallFromFighting = true;
				MainScreen.bag.show_bag();
				while(true)
				{	
					if(Bag.selected == false && Bag.ReturnFromESC==false)
					{
						try{
						Thread.sleep(20);
						}
						catch(InterruptedException ie){}
					}
					else
						break;
				}
							
				if(Bag.selected)
					MainScreen.bag.backToMainScreen();
					
				if(Bag.ReturnFromESC==true){
					info = new ActionAndTarget(0,-1);	
					remove(menu);
					menu = new AttackMenu(info);
					menu.setBounds(LosersAndWinner.screenWidth-350,LosersAndWinner.screenHeight-350,250,250);
					add(menu,0);
					repaint();			
					continue;
				}
				MainScreen.bag.CallFromFighting = false;		
				break;
			}
			else
				break;
		}
		remove(mc_arrow);
		remove(menu);
		repaint();
	}
	/* Choosing by static var. "target'*/
	public void ChoosingTarget(ActionAndTarget info)
	{
		int target;
		JLabel arrow = new TargetArrow(info);
		arrow.setBounds(monst_pos[0][0]-15+50, monst_pos[0][1]-50, 30, 50);
		add(arrow,0);
		repaint();		
		
		while(true)
		{
			target = info.target;//updating target
			if(target  == -1){
				try{
					Thread.sleep(500);
				}
				catch(InterruptedException ie){}
			}			
			else
				break;

		}
		remove(arrow);
		repaint();
	}
	
	public void Monst_Turn(int whose_turn)
	{
		System.out.println("Monster #"+whose_turn+" attacking");
	}
	
	class AttackMenu extends JLabel
	{
		int state;
		ActionAndTarget info;
		AttackMenuListener temp_listener;
		
		public AttackMenu(ActionAndTarget i)
		{
			info = i;
			state = i.action;
			
			setIcon(AllIcons.menu0);
			
			temp_listener = new AttackMenuListener();
			FightingScreen.mainJF.addKeyListener(temp_listener);
		}

		class AttackMenuListener extends KeyAdapter
		{
			public void keyPressed(KeyEvent e) 
			{
				if(state !=0 &&(e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE))
				{
					info.action = state;
					FightingScreen.mainJF.removeKeyListener(temp_listener);
				}
				else if(e.getKeyCode() == KeyEvent.VK_UP)
				{
					state = 1;
					setIcon(AllIcons.menu1);
				}
				else if(e.getKeyCode() == KeyEvent.VK_LEFT)
				{	
					if(state == 5)
					{
						state = 4;
						setIcon(AllIcons.menu4);
					}
					else
					{
						state = 2;
						setIcon(AllIcons.menu2);
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_RIGHT)
				{
					if(state == 4)
					{
						state = 5;
						setIcon(AllIcons.menu5);
					}
					else
					{
						state = 3;
						setIcon(AllIcons.menu3);
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_DOWN)
				{
					if(state == 3)
					{
						state = 5;
						setIcon(AllIcons.menu5);
					}
					else
					{	
						state= 4;
						setIcon(AllIcons.menu4);
					}
				}
				//add ESC option
			}
		}
	}
	
	
	
	class TargetArrow extends JLabel
	{
		int selected;
		ActionAndTarget info;
		TargetListener temp_listener;
		
		public TargetArrow(ActionAndTarget i)
		{
			info = i;
			selected = 0;//i.target;
			setIcon(AllIcons.Arrow);
			temp_listener = new TargetListener();
			FightingScreen.mainJF.addKeyListener(temp_listener);
		}
				
		
		class TargetListener extends KeyAdapter
		{
			public void keyPressed(KeyEvent e) 
			{
				if(e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE)
				{
					info.target = selected;;
					FightingScreen.mainJF.removeKeyListener(temp_listener);
				}
				else
				{
					selected = (selected+1)%2;
					setBounds(FightingScreen.monst_pos[selected][0]-15+50, FightingScreen.monst_pos[selected][1]-50, 30, 50);
				}
			}				
		}
	
	}
	
	class ActionAndTarget
	{
		public int action;
		public int target;
		
		public ActionAndTarget(int a, int t)
		{
			action = a;
			target = t;	
		}
	}
}