import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class Fighting_CharacterInfo extends JPanel
{
	private static final int Info_w = 240;
	private static final int Info_h = 360;
	
	public static final JButton redbtn   = new JButton();
	public static final JButton greenbtn = new JButton();
	static{
		redbtn.setBackground(Color.red);
		greenbtn.setBackground(Color.green);
	}
	
			
	MC character;
	int HP, MP;
	
	JButton jb;
	FightingScreen fs;
			
	public Fighting_CharacterInfo(MC mc)
	{
		character = mc;
		//jb.setBackground(mc.getBigIcon());
		redbtn.setBackground(Color.red);
		greenbtn.setBackground(Color.green);
		
		this.setLayout(null);
		this.setBounds(0, 0, Info_w ,Info_h);
	}
	
} 