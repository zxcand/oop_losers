import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class Fighting_ProgBar extends JPanel implements Runnable{	
	//the progress bar is running or not
	private JLabel ProgLabel;
	private FightingScreen fs;

	private static boolean go = true;
	private static boolean end = false;
	
	
	public static synchronized  void stop(){ go = false;}
	public static synchronized  void start(){ go = true;}
	public static synchronized  boolean isGoing(){ return go;}
	
	public static synchronized void END() { end = true;}
	public static synchronized void RESUME() { end = false;}
	public static synchronized boolean isEND() { return end;}
	
	public Fighting_ProgBar(FightingScreen fs){
		this.setLayout(null);
		this.setOpaque(false);
		this.fs = fs;
		
		ProgLabel = new JLabel(AllIcons.ProgBar);
		ProgLabel.setBounds(0,0,FightingScreen.bar_width ,FightingScreen.bar_height);
		this.add(ProgLabel,0);
	}
	
	public void run(){
		Thread[] t = new Thread[5];
		for(int i=0; i<3; i++){
			CharacterOnBar head = new CharacterOnBar(i);
			head.setBounds(0,0,100,100);
			add(head,0);
			revalidate();
			
			t[i] = new Thread(head);
			t[i].start();	
			
			try{
					Thread.sleep(100);
			}
			catch(InterruptedException ie){};
			
		}
		
		for(int i=0; i<fs.Monst_Num; i++){
			MonstOnBar head = new MonstOnBar(i);
			head.setBounds(0,0,100,100);
			add(head,0);
			revalidate();
			
			t[i+3] = new Thread(head);
			t[i+3].start();
			
			try{
				Thread.sleep(500);
			}
			catch(InterruptedException ie){};
		}
		
		//Wait for all threads terminate(���"�M")
		try{
			t[0].join();
			t[1].join();
			t[2].join();
			t[3].join();
			t[4].join();
			
        } catch (InterruptedException e) {}
		
	}

	
	
	/*MC on the prog bar*/
	class CharacterOnBar extends JLabel implements Runnable{	
		int pos;
		int id;
		public CharacterOnBar(int n){
			pos = 0;
			id = n;
			setIcon(MainScreen.MCList[id].getFightingIcon());
		}	
		
		public void run(){
			while(true){
				if(Fighting_ProgBar.isEND())
					break;
				if(Fighting_ProgBar.isGoing()){					
					pos += MainScreen.MCList[id].getAGI();
					setBounds(pos,0,100,100);
					repaint();
					if(pos > FightingScreen.bar_width-100){
						Fighting_ProgBar.stop();
						fs.MC_Turn(id);
						pos = 0;		
						Fighting_ProgBar.start();
					}
				}
				
				try{
					Thread.sleep(20);
				}
				catch(InterruptedException ie){};
			}
		}	
	}

	/*Monst on prog bar*/
	class MonstOnBar extends JLabel implements Runnable{	
		int pos;
		int id;
		
		public MonstOnBar(int n){
			pos = 0;
			id = n;
			setIcon(FightingScreen.MonsterList.get(id).BarIcon);
		}	
		
		public void run(){
			while(true){
				if(Fighting_ProgBar.isEND())
					break;
				if(Fighting_ProgBar.isGoing()){					
					pos += 3;
					setBounds(pos,0,100,100);
					repaint();
					if(pos > FightingScreen.bar_width-100){
						Fighting_ProgBar.stop();
						//fs.Turn();  Monst_turn
						pos = 0;		
						Fighting_ProgBar.start();
					}
				}
				
				try{
					Thread.sleep(20);
				}
				catch(InterruptedException ie){};
			}
		}
	}
}


