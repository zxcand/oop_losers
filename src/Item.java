/* 所有的道具都會繼承Item
method:
public void set_money(int _money)
public void set_name(String _name)
public void set_icon(ImageIcon _icon) 設置道具的圖示
public void set_description(String _description) 設置對這個item的描述，察看道具時會顯示
public int get_money() 道具的價格
public String get_name() 
public String get_description() 得到對這道具的文字描述
public ImageIcon get_icon() 得到道具的圖示
public void use(MC mc)
public void use(Monster monster)
*/

import java.awt.*;
import javax.swing.*;
import java.util.*;

public abstract class Item {
	private int money;
	private String name;
	private ImageIcon icon;
	private String description;
	public void set_money(int _money) {
		money = _money;
	}
	public void set_name(String _name) {
		name = _name;
	}
	public void set_icon(ImageIcon _icon) {
		icon = _icon;
	}
	public void set_description(String _description) {
		description = _description;
	}
	public int get_money() {
		return money;
	}
	public String get_name() {
		return name;
	}
	public ImageIcon get_icon() {
		return icon;
	}
	public String get_description() {
		return description;
	}
	public abstract void use(MC mc);
	/*
	public abstract void use(Monster monster) {
	}*/
}
class Red_water extends Item {
	public Red_water() {
		super.set_money(100);
		super.set_name("紅色藥水");
		super.set_description("<html>使用後可恢復100點HP。<br> 售價:100 </html>");
		super.set_icon(AllIcons.red_water_icon);
		
		//super.set_icon(red_water_icon);
	}
	public void use(MC mc) {
		mc.setHP(mc.getHP()+100);
	}
	/*
	public void use(Monster monster) {
	}*/
}
class Blue_water extends Item {
	public Blue_water() {
		super.set_money(100);
		super.set_name("藍色藥水");
		super.set_description("<html>使用後可恢復100點MP。<br> 售價:100 </html>");
		//super.set_icon(blue_water_icon);
		super.set_icon(AllIcons.blue_water_icon);
	}
	public void use(MC mc) {
		mc.setMP(mc.getMP()+100);
	}
	//public void use(Monster monster) {
	//}
}