import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
public class LosersAndWinner extends JFrame{
	//private JFrame mainFrame;
	protected static int screenWidth,screenHeight;
	private Dimension dimension = new Dimension();
	protected static LosersAndWinner mainFrame;
	
	//protected KeyAdapter frameHandler;
	public LosersAndWinner(){
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Rectangle bounds = ge.getMaximumWindowBounds();
		this.setBounds(bounds);
		dimension = Toolkit.getDefaultToolkit().getScreenSize();
		screenWidth=(int)dimension.getWidth();
		screenHeight=(int)dimension.getHeight();
		this.setUndecorated(true);		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(screenWidth,screenHeight);
		this.getContentPane().setLayout(null);
		this.getContentPane().setBackground(Color.black);
		this.setVisible(true);
		
		new login(this);
		//this.add(new MainScreen(this));
	}
	public static void main(String[] args){
		mainFrame = new LosersAndWinner();
	}
}