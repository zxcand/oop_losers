import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
public class MC {
	private int HP;
	private int MAX_HP;
	private int MP;
	private int MAX_MP;
	private int LV;
	private int EXP;
	private int MAX_EXP;
	private int ATT;
	private int DEF;
	private int AGI;
	private int money;
	private LinkedList<Skill> skill = new LinkedList<Skill>();
	private ImageIcon FightingIcon;
	private ImageIcon MCIcon;
	private int MC_num;
	private String Name;
	private Weapon[] equipment = new Weapon[4];
	public MC(int _MC_num) { 
		MC_num = _MC_num;
		//MCIcon = _MCIcon;
		Name = "Dick";
		MAX_HP = 50;
		HP = 50;
		MAX_MP = 50;
		MP = 50;
		LV = 1;
		EXP = 0;
		MAX_EXP = 50+LV*15;
		ATT = 10;
		DEF = 5;
		AGI = 5;
		money = 100;
		
		FightingIcon = new ImageIcon("../image/fighting/a"+MC_num+".png");
	}
	public String getName() {return Name;}
	public int getHP() {return HP;}
	public int getMAXHP() {return MAX_HP;}
	public int getMP() {return MP;}
	public int getMAXMP() {return MAX_MP;}
	public int getAGI() {return AGI;}
	public int getATT() {return ATT;}
	public int getDEF() {return DEF;}
	public int getMoney() {return money;}
	public ImageIcon getFightingIcon() {return FightingIcon;}
	
	public void setHP(int _HP) {
		HP=_HP;
		if(HP>MAX_HP) {
			HP = MAX_HP;
		}
	}
	public void setMAXHP(int _MAX_HP) {MAX_HP = _MAX_HP;}
	public void setMP(int _MP) {
		MP=_MP;
		if(MP>MAX_MP) {
			MP = MAX_MP;
		}
	}
	public void setMAXMP(int _MAX_MP) {MAX_MP = _MAX_MP;}
	public void setAGI(int _AGI) {AGI=_AGI;}
	public void setATT(int _ATT) {ATT=_ATT;}
	public void setDEF(int _DEF) {DEF=_DEF;}
	public void setMoney(int _money) {money = _money;}
	public void setEXP(int _EXP) {
		EXP = EXP+_EXP;
		while(EXP>=MAX_EXP) {
			LV++;
			EXP = EXP+_EXP-MAX_EXP;
			//ATT DEF DEX increase(can select by player)
			//MAX_EXP HP MP increase itself(by formula)
		}
	}
	public boolean equip(Weapon weapon) {
		if(weapon.get_equiped()==true) {
			return false;
		}
		if(equipment[weapon.get_type()]!=null) {
			equipment[weapon.get_type()].unequip(this);
			equipment[weapon.get_type()].set_equiped(false,null);
		}
		equipment[weapon.get_type()] = weapon;
		weapon.equip(this);
		return true;
	}
	public boolean unequip(Weapon weapon) {
		if(equipment[weapon.get_type()]==null && !equipment[weapon.get_type()].equals(weapon)) {
		   return false;
		}
		equipment[weapon.get_type()]=null;
		weapon.unequip(this);
		return false;
	}
}

class Skill{
	public Skill() {
	}
	
	
}
		
		
		
		