/**
非戰鬥主畫面
包含各種主要Method
LoadMap(int episode):將地圖檔案(JPanel)讀入。使用episode指定地圖檔案
LoadTinyMap():讀入小地圖
Teleport():轉換地圖
KeyHandler:包含移動指令(上 下 左 右) 對話指令(space) 暫停指令(ESC) 切換人物(A)
*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.text.*;
public class MainScreen extends JLayeredPane{
	private static final int TINYMAPSIZE = 200;
	private static final int MOVESPEED = 10;
	private Random rand = new Random();
	private characterIcon MCIcon = new characterIcon();
	private int MCNum=2;
	private JLabel MCLabel = new JLabel();
	private JLabel timeLabel = new JLabel();
	private JLabel nightLabel = new JLabel(AllIcons.night);
	protected Map_Object mapPanel;
	private TinyMap TinyMapPanel;
	private ESCMenu escMenu;
	protected MainScreen mainScreen = this;
	protected int screenWidth,screenHeight;
	protected MapKeyHandler handler = new MapKeyHandler();
	private LosersAndWinner frame;
	private int timeCount;
	private int teleportColdDown;
	private Thread timeThread;
	private Thread mapTagThread;
	private Thread walkThread;
	private Thread fightThread;
	private NPC[] NPCList = new NPC[50];
	protected static Bag bag;
	protected static MC[] MCList = new MC[3];//called by fighting screen(改成list,以便因應人數變動)
	private int fight;
	
	static{
		MCList[0] = new MC(0);
		MCList[1] = new MC(1);
		MCList[2] = new MC(2);
		
		
		//testing
		MCList[0].setAGI(3);
		MCList[1].setAGI(5);
		MCList[2].setAGI(7);
	}
	public MainScreen(LosersAndWinner _frame){
		frame = _frame;
		screenWidth=frame.screenWidth;
		screenHeight=frame.screenHeight;
		this.setSize(screenWidth,screenHeight);
		this.setLayout(null);
		this.setBackground(Color.black);
		createObj();
		/*First Map Load in*/
		LoadMap(1);
		
		addMC();
		frame.addKeyListener(handler);
		addTimeLabel();
	}
	private void addMC(){
		this.add(MCLabel,100);
		MCLabel.setBounds(screenWidth/2-25,screenHeight/2-25,50,50);
		MCLabel.setIcon(MCIcon.MC[MCNum][1][1]);
		MCLabel.setOpaque(false);
		this.moveToFront(MCLabel);
	}
	private void addTimeLabel(){
		this.add(timeLabel,100);
		timeLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
		timeLabel.setForeground(Color.white);
		timeLabel.setBounds(0,0,200,30);
		this.moveToFront(timeLabel);
		CountTimeStart();
	}
	private void createObj(){
		escMenu = new ESCMenu(this,frame);
		//mapSet = new MapSet();
		bag = new Bag(frame,handler);
		bag.add_weapon((Weapon)(new Long_sword1()));
		bag.add_weapon((Weapon)(new Long_sword1()));
		bag.add_item((Item)(new Red_water()),3);
		bag.add_item((Item)(new Blue_water()),5);
		bag.add_weapon((Weapon)(new Long_sword1()));
	}
	public void CountTimeStart(){
		timeThread = new Thread(new Runnable() {
            public void run(){
                while(true){
					timeCount++;
					if(teleportColdDown>0)
						teleportColdDown--;
					try{ 
						timeThread.sleep(1000); 
					}
					catch(InterruptedException e){
						System.out.println("the sleep of timeThread interrupted");
					}
					setTimeLabel();
				}
				
            }
		});
		timeThread.start();
	}
	public void setTimeLabel(){
		int second = timeCount%60;
		int minute = (timeCount/60)%60;
		int hour = timeCount/3600;
		timeLabel.setText(String.format("%d%d:%d%d:%d%d",hour/10,hour%10,minute/10,minute%10,second/10,second%10));
	}
	public void LoadMap(int episode){
		BGMPlayer.BGMclip.stop();
		mapPanel = MapSet.Map[episode];
		try{
			BGMPlayer.playBGM(mapPanel.BGMName);
		}catch(Throwable t){}
		this.add(mapPanel,0);
		mapPanel.setScreenPos();
		mapPanel.setBounds(mapPanel.x,mapPanel.y,mapPanel.MAPSIZE,mapPanel.MAPSIZE);
		mapPanel.setBackground(Color.black);
		LoadNPC();
		this.moveToFront(MCLabel);
		NightSystem();
		LoadTinyMap();
		this.moveToFront(timeLabel);
		//ShowMapTag();
	}
	public void NightSystem(){
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("HH");  
		String currentDatetime = dateFormat.format(date);
		if(Integer.parseInt(currentDatetime)>18 || Integer.parseInt(currentDatetime)<6){
			this.add(nightLabel,0);
			nightLabel.setBounds(0,0,2000,2000);
		}
	}
	public void LoadNPC(){
		for(int i=0 ; i<mapPanel.MAPSIZE/50 ; i++)
			for(int j=0 ; j<mapPanel.MAPSIZE/50 ; j++){
				if(mapPanel.mapInfo[i][j] >25 && mapPanel.mapInfo[i][j] < 41){
					NPCList[mapPanel.mapInfo[i][j]-26] = new NPC(this,frame,mapPanel.mapInfo[i][j]-26);
					NPCList[mapPanel.mapInfo[i][j]-26].setPosition(i,j);
					mapPanel.add(NPCList[mapPanel.mapInfo[i][j]-26],0);
					NPCList[mapPanel.mapInfo[i][j]-26].setBounds(i*50,j*50,50,50);
				}
			}
	}
	public void LoadTinyMap(){
		TinyMapPanel = new TinyMap(mapPanel.mapName,mapPanel.MAPSIZE);
		this.add(TinyMapPanel,0);
		TinyMapPanel.setBounds(screenWidth-TINYMAPSIZE,0,TINYMAPSIZE,TINYMAPSIZE);
		TinyMapPanel.setBackground(Color.black);
		TinyMapPanel.Move(mapPanel.characterX/50,mapPanel.characterY/50);
	}
	/*public void ShowMapTag(){
		mapTagThread = new Thread(new Runnable() {
            public void run(){
				JLabel mapTagLabel = new JLabel(LoserVillageTag);
                frame.add(mapTagLabel,0);
				mapTagLabel.setBounds((screenWidth-1000)/2,(screenHeight-300)/2,1000,300);
				try{ 
					mapTagThread.sleep(3000); 
				}
				catch(InterruptedException e){
					System.out.println("the sleep of timeThread interrupted");
				}
				frame.remove(mapTagLabel);
				frame.repaint();
            }
		});
		mapTagThread.start();
	}*/
	public void Teleport(int teleportNum , int x , int y){
		teleportColdDown=2;
		this.remove(mapPanel);
	    this.remove(TinyMapPanel);
		repaint();
		LoadMap(teleportNum);
	}
	public void EventWalk(){
		walkThread = new Thread(new Runnable(){
			public void run(){
				for(int i=0 ; i<10 ; i++){
					mapPanel.y-=MOVESPEED;
					mapPanel.characterY+=MOVESPEED;
					MCIcon.CountStep();
					MCLabel.setIcon(MCIcon.MC[MCNum][1][(int)(MCIcon.step/3)]);
					MCIcon.CountStep();
					MCLabel.setIcon(MCIcon.MC[MCNum][1][(int)(MCIcon.step/3)]);
					MCIcon.CountStep();
					MCLabel.setIcon(MCIcon.MC[MCNum][1][(int)(MCIcon.step/3)]);
					mapPanel.setBounds(mapPanel.x,mapPanel.y,mapPanel.MAPSIZE,mapPanel.MAPSIZE);
					try{
						walkThread.sleep(100);
					}
					catch(InterruptedException e){
					}
				}
			}
		});
		walkThread.start();
	}
	public void Fight(){
		frame.removeKeyListener(handler);
		fightThread = new Thread(new Runnable() {
            public void run(){
				int x,y,time=0;
				JPanel blockPanel = new JPanel();
				frame.add(blockPanel,0);
				blockPanel.setBounds(0,0,screenWidth,screenHeight);
				blockPanel.setLayout(null);
				blockPanel.setOpaque(false);
				while(time<30){
					time++;
					x=rand.nextInt(40);
					y=rand.nextInt(40);
					mapPanel.setLocation(mapPanel.x+20-x,mapPanel.y+20-y);
					try{ 
						fightThread.sleep(10); 
					}
					catch(InterruptedException e){
						}
				}
				mapPanel.setLocation(mapPanel.x,mapPanel.y);
				time=0;
				while(time<10){
					time++;
					JLabel blockLabel = new JLabel(AllIcons.block);
					blockPanel.add(blockLabel);
					blockLabel.setBounds(0,0,2000,2000);
					try{ 
						fightThread.sleep(5); 
					}
					catch(InterruptedException e){
					}
				}
				frame.add(new FightingScreen(frame,handler),0);
				frame.remove(blockPanel);
				frame.repaint();
            }
		});
		fightThread.start();
		this.moveToFront(timeLabel);
		BGMPlayer.BGMclip.stop();
		try{
			BGMPlayer.playBattleBGM();
		}catch(Throwable t){}
	}
	class MapKeyHandler extends KeyAdapter{
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == e.VK_DOWN){
				MCLabel.setIcon(MCIcon.MC[MCNum][1][1]);
				if(mapPanel.mapInfo[(int)(mapPanel.characterX/50)][(int)((mapPanel.characterY+(MOVESPEED+10))/50)] >= 0
				    && mapPanel.mapInfo[(int)(mapPanel.characterX/50)][(int)((mapPanel.characterY+(MOVESPEED+10))/50)] < 26){
					mapPanel.y-=MOVESPEED;
					mapPanel.characterY+=MOVESPEED;
					MCIcon.CountStep();
					MCLabel.setIcon(MCIcon.MC[MCNum][1][(int)(MCIcon.step/3)]);
				}
			}
			else if(e.getKeyCode() == e.VK_UP){
				MCLabel.setIcon(MCIcon.MC[MCNum][0][1]);
				if(mapPanel.mapInfo[(int)(mapPanel.characterX/50)][(int)((mapPanel.characterY-MOVESPEED)/50)] >= 0
				   && mapPanel.mapInfo[(int)(mapPanel.characterX/50)][(int)((mapPanel.characterY-MOVESPEED)/50)] <26){
					mapPanel.y+=MOVESPEED;
					mapPanel.characterY-=MOVESPEED;
					MCIcon.CountStep();
					MCLabel.setIcon(MCIcon.MC[MCNum][0][(int)(MCIcon.step/3)]);
				}
			}
			else if(e.getKeyCode() == e.VK_RIGHT){
				MCLabel.setIcon(MCIcon.MC[MCNum][3][1]);
				if(mapPanel.mapInfo[(int)((mapPanel.characterX+(MOVESPEED+10))/50)][(int)(mapPanel.characterY/50)] >= 0
				    && mapPanel.mapInfo[(int)((mapPanel.characterX+(MOVESPEED+10))/50)][(int)(mapPanel.characterY/50)] <26){
					mapPanel.x-=MOVESPEED;
					mapPanel.characterX+=MOVESPEED;
					MCIcon.CountStep();
					MCLabel.setIcon(MCIcon.MC[MCNum][3][(int)(MCIcon.step/3)]);
				}
			}
			else if(e.getKeyCode() == e.VK_LEFT){
				MCLabel.setIcon(MCIcon.MC[MCNum][2][1]);
				if(mapPanel.mapInfo[(int)((mapPanel.characterX-(MOVESPEED+10))/50)][(int)(mapPanel.characterY/50)] >= 0 
				    && mapPanel.mapInfo[(int)((mapPanel.characterX-(MOVESPEED+10))/50)][(int)(mapPanel.characterY/50)] < 26){
					mapPanel.x+=MOVESPEED;
					mapPanel.characterX-=MOVESPEED;
					MCIcon.CountStep();
					MCLabel.setIcon(MCIcon.MC[MCNum][2][(int)(MCIcon.step/3)]);
				}
			}
			else if(e.getKeyCode() == e.VK_A){
				MCNum++;
				if(MCNum >= 3)
					MCNum=0;
				MCLabel.setIcon(MCIcon.MC[MCNum][1][1]);
			}
			else if(e.getKeyCode() == e.VK_SPACE){
				if(mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50-1] >25 && mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50-1] <41){
				    NPCList[mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50-1]-26].talk();
				}
			}
			else if(e.getKeyCode() == e.VK_ESCAPE){
				escMenu.CallESCMenu();
			}
			else if(e.getKeyCode() == e.VK_B){
				bag.show_bag();
			}
			else if(e.getKeyCode() == e.VK_F){
				fight^=1;
			}
			mapPanel.setBounds(mapPanel.x,mapPanel.y,mapPanel.MAPSIZE,mapPanel.MAPSIZE);
			TinyMapPanel.Move(mapPanel.characterX/50,mapPanel.characterY/50);
			
			if(mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50] >0 && mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50] <= 20 && teleportColdDown==0)
				Teleport(mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50],mapPanel.characterX,mapPanel.characterY);
			if(mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50] >20&&mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50] <26)
				if(EventRecorder.event[mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50]-21]==0)
					EventCaller.Event(mapPanel.mapInfo[mapPanel.characterX/50][mapPanel.characterY/50]-21,mainScreen);
			if(rand.nextInt(1000) < 10)
				if(fight==1)
					Fight();
		}
	}
	
}

     