import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
public class MapEditor{
	private int x=1200;
	private int y=800;
	private int ImageHeight;
	private int ImageWidth;
	ImageIcon Map;
	ImageIcon Cross = new ImageIcon("../image/cross.png");
	ImageIcon Teleport = new ImageIcon("../image/teleport.png");
	ImageIcon N = new ImageIcon("../image/N.png");
	private JLabel mapLabel;
	private JPanel mapPanel = new JPanel();
	private JScrollPane scrollPanel;
	private JLabel BGLabel;
	private JLabel[][] editorLabel;
	private JLabel commandLabel = new JLabel();
	private JButton[] controlButton = new JButton[6];
	private String imageName;
	private String mapName;
	//private String outputName;
	private Map_Object editmap = new Map_Object();
	private JFrame mainFrame;
	private int command;
	private int openOrCreate;
	private Scanner scanner = new Scanner(System.in);
	//private ButtonListener BL = new ButtonListener();
	public static void writeObjectsToFile(Object obj, String filename){
		File file = new File(filename);
		if(!file.exists()){
			// file.getParentFile().mkdirs();
			try {
				file.createNewFile();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		try{
			ObjectOutputStream objOutputStream = new ObjectOutputStream(new FileOutputStream(file)); 
			objOutputStream.writeObject(obj);
			objOutputStream.close();
		}
		catch(IOException e) { 
			e.printStackTrace(); 
		}
	}
	public void readObjectsFromFile(String filename)throws FileNotFoundException{
		File file = new File("../map/"+filename);
		if(!file.exists()) 
			throw new FileNotFoundException();
		try{
			FileInputStream fileInputStream = new FileInputStream(file);
			ObjectInputStream objInputStream = new ObjectInputStream(fileInputStream); 
			while(fileInputStream.available() > 0) {
				editmap = ((Map_Object)objInputStream.readObject());
			}
			objInputStream.close();
		}
		catch(ClassNotFoundException e) { 
			e.printStackTrace(); 
		} 
		catch(IOException e) { 
			e.printStackTrace(); 
		}
		return;
	}
	public void setMapObject(){
		
		System.out.println("(1)新建地圖 (2)開啟舊檔:");
		openOrCreate=scanner.nextInt();
		if(openOrCreate==1)
			CreateMap();
		else if(openOrCreate==2)
			OpenMap();
		
	}
	public void CreateMap(){
		System.out.println("圖檔檔名:");
		imageName = scanner.next();
		System.out.println("地圖大小:");
		editmap.setMAPSIZE(scanner.nextInt());
		ImageWidth=editmap.MAPSIZE;
		ImageHeight=editmap.MAPSIZE;
		System.out.println("地圖名稱:");
		editmap.setMapName(scanner.next());
		System.out.println("背景音樂檔名:");
		editmap.setBGMName(scanner.next());
		System.out.println("角色起始位置:");
		System.out.println("X:");
		editmap.setCharacterX(scanner.nextInt());
		System.out.println("Y:");
		editmap.setCharacterY(scanner.nextInt());
		editmap.createMapArray(ImageWidth);
		Map = new ImageIcon("../image/map/"+imageName);
		BGLabel = new JLabel(Map);
		BGLabel.setHorizontalAlignment(JLabel.LEFT);
		BGLabel.setVerticalAlignment(JLabel.TOP);
		editmap.add(BGLabel);
		BGLabel.setBounds(0,0,ImageWidth,ImageHeight);
		
		
	}
	public void changeMapInfo(){
		System.out.println("地圖大小:");
		editmap.setMAPSIZE(scanner.nextInt());
		ImageWidth=editmap.MAPSIZE;
		ImageHeight=editmap.MAPSIZE;
		System.out.println("地圖名稱:");
		editmap.setMapName(scanner.next());
		System.out.println("背景音樂檔名:");
		editmap.setBGMName(scanner.next());
		System.out.println("角色起始位置:");
		System.out.println("X:");
		editmap.setCharacterX(scanner.nextInt());
		System.out.println("Y:");
		editmap.setCharacterY(scanner.nextInt());
	}
	public void OpenMap(){
		System.out.println("地圖檔檔名:");
		mapName = scanner.next();
		try{
			readObjectsFromFile(mapName);
		}	
		catch(FileNotFoundException e){
			System.out.println("No file!!");
		}
		System.out.println("是否修改地圖基本資料?(y/n)");
		if(scanner.next().equals("y"))
			changeMapInfo();
	}
	public MapEditor(){
		setMapObject();
		
		
		mainFrame = new JFrame();
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setSize(x,y);
		mainFrame.getContentPane().setLayout(null);
		mainFrame.getContentPane().setBackground(Color.black);
		mapLabel = new JLabel();
		editmap.add(mapLabel,0);
		mapLabel.setBounds(0,0,editmap.MAPSIZE,editmap.MAPSIZE);
		//mapPanel.setLayout(null);
		//mapPanel.setSize(2000,2000);
		//mapPanel.add(mapLabel);
		System.out.println(editmap.MAPSIZE);
		editmap.setPreferredSize(new Dimension(editmap.MAPSIZE,editmap.MAPSIZE));
		//editmap.setSize(editmap.MAPSIZE,editmap.MAPSIZE);
	//	scrollPanel.setLayout(null);
		//mapLabel.setBounds(0,0,ImageWidth,ImageHeight);
		scrollPanel = new JScrollPane((JPanel)editmap);
		mainFrame.add(scrollPanel);
		scrollPanel.setBounds(0,0,800,600);
		//scrollPanel.add(mapLabel);
		//mapLabel.setBounds(0,0,ImageWidth,ImageHeight);
		mainFrame.setVisible(true);
		mainFrame.add(commandLabel);
		mapLabel.addMouseListener(new MouseHandler());
		scrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		commandLabel.setBounds(1000,0,200,50);
		controlButton[0] = new JButton("-");
		controlButton[1] = new JButton("+");
		controlButton[2] = new JButton("SAVE");
		/*controlButton[3] = new JButton("-1");
		controlButton[4] = new JButton("-5");
		controlButton[5] = new JButton("-10");*/
		for(int i=0 ; i<3 ; i++){
			mainFrame.add(controlButton[i]);
			controlButton[i].setBounds(1000+i*60,100,60,70);
		}
		controlButton[0].addActionListener(new ButtonListener(-1));
		controlButton[1].addActionListener(new ButtonListener(1));
		controlButton[2].addActionListener(new ButtonListener(10));
		/*controlButton[3].addActionListener(new ButtonListener(-1));
		controlButton[4].addActionListener(new ButtonListener(-5));
		controlButton[5].addActionListener(new ButtonListener(-10));*/
		editorLabel = new JLabel[editmap.MAPSIZE/50][editmap.MAPSIZE/50];
		for(int i=0 ; i<editmap.MAPSIZE/50 ; i++)
			for(int j=0 ; j<editmap.MAPSIZE/50 ; j++){
				editorLabel[i][j] = new JLabel();
				mapLabel.add(editorLabel[i][j],0);
				editorLabel[i][j].setBounds(i*50,j*50,50,50);
			}
		setMapLabel();
	}
	public static void main(String[] args){
		new MapEditor();
	}
	public void setMapLabel(){
		for(int i=0 ; i<editmap.MAPSIZE/50 ; i++)
			for(int j=0 ; j<editmap.MAPSIZE/50 ; j++){
				if(editmap.mapInfo[i][j]==-1){                      //obstacle	
					editorLabel[i][j].setIcon(Cross);
					mainFrame.repaint();
				}
				else if(editmap.mapInfo[i][j]==0){                  //road
					editorLabel[i][j].setIcon(null);
					editorLabel[i][j].setText(null);
					mainFrame.repaint();
				}
				else if(editmap.mapInfo[i][j]>0 && editmap.mapInfo[i][j] < 21){   //teleport
					editorLabel[i][j].setIcon(Teleport);
					mainFrame.repaint();
				}
				else if(editmap.mapInfo[i][j]>20 && editmap.mapInfo[i][j] < 41){  //NPC
					editorLabel[i][j].setText(Integer.toString(editmap.mapInfo[i][j]));
					editorLabel[i][j].setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
					editorLabel[i][j].setHorizontalAlignment(SwingConstants.CENTER);
					editorLabel[i][j].setVerticalAlignment(SwingConstants.CENTER);
					mainFrame.repaint();
				}
			}
	}
	private class ButtonListener implements ActionListener{
		private int count;
		public ButtonListener(int c){
			count=c;
		}
		public void actionPerformed(ActionEvent e){
			if(count == 10){
				editmap.remove(mapLabel);
				writeObjectsToFile(editmap,"../map/"+editmap.mapName);
			}
			else{
				command+=count;
				commandLabel.setText("command="+Integer.toString(command));
			}
		}
    }
	class MouseHandler extends MouseAdapter implements MouseListener , MouseMotionListener{
			int eventX;
			int eventY;
			public void mouseClicked( MouseEvent event ){
				eventX=event.getX();
				eventY=event.getY();
				if(eventX>=25)
					eventX-=25;
				if(eventY>=25)
					eventY-=25;
				System.out.println(eventX);
				System.out.println(eventY);
				if(eventX <editmap.MAPSIZE && eventY<editmap.MAPSIZE){
					if(editmap.mapInfo[eventX/50][eventY/50]!=command){
						editmap.mapInfo[eventX/50][eventY/50] = command;
						if(command==-1){                      //obstacle
							
							editorLabel[eventX/50][eventY/50].setIcon(Cross);
							mainFrame.repaint();
						}
						else if(command==0){                  //road
							editorLabel[eventX/50][eventY/50].setIcon(null);
							editorLabel[eventX/50][eventY/50].setText(null);
							mainFrame.repaint();
						}
						else if(command>0 && command < 21){   //teleport
							editorLabel[eventX/50][eventY/50].setIcon(Teleport);
							mainFrame.repaint();
						}
						else if(command>20 && command < 41){  //NPC
							editorLabel[eventX/50][eventY/50].setText(Integer.toString(command));
							editorLabel[eventX/50][eventY/50].setFont(new Font("Comic Sans MS", Font.PLAIN, 20));
							editorLabel[eventX/50][eventY/50].setHorizontalAlignment(SwingConstants.CENTER);
							editorLabel[eventX/50][eventY/50].setVerticalAlignment(SwingConstants.CENTER);
							mainFrame.repaint();
						}
					}
				}					
			}
	 }
}