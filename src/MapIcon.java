/**
儲存所有地圖物件的圖片(ImageIcon)
*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
public class  MapIcon{
	protected static ImageIcon NPC[] = new ImageIcon[10];
	protected static ImageIcon House[] = new ImageIcon[10];
	protected static ImageIcon Item[] = new ImageIcon[10];
	protected static ImageIcon Decoration[] = new ImageIcon[10];
	protected static ImageIcon Background[] = new ImageIcon[10];
	protected static ImageIcon RoomIndoor[] = new ImageIcon[10];
	
	static{
		NPC[0] = new ImageIcon("../image/Character/MC2_D2.png");
		//////////////////////////////////////////////////////////
		House[0] = new ImageIcon("../image/mapHouse.png");
		House[1] = new ImageIcon("../image/tower.png");
		////////////////////////////////////////////////////////////
		Decoration[0] = new ImageIcon("../image/mapTree.jpg");
		Decoration[1] = new ImageIcon("../image/mapStone.jpg");
		Decoration[2] = new ImageIcon("../image/hole.png");
		Decoration[3] = new ImageIcon("../image/pool_150x150");
		Decoration[4] = new ImageIcon("../image/pool_100x50");
		Decoration[5] = new ImageIcon("../image/mountain_150x150");
		////////////////////////////////////////////////////////////
		Background[0] = new ImageIcon("../image/village1.png");
		Background[1] = new ImageIcon("../image/mapGrass&Flower.jpg");
		Background[2] = new ImageIcon("../image/grass2.png");
		Background[3] = new ImageIcon("../image/gray_ground.png");
		Background[4] = new ImageIcon("../image/brown_ground.png");
		////////////////////////////////////////////////////////////
		RoomIndoor[0] = new ImageIcon("../image/Room1.png");
	}
}