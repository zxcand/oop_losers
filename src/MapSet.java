/**
儲存所有MAP物件，依照陣列index來指定map
*/
import java.util.*;
import java.io.*;
public class MapSet{
	protected static Map_Object Map[] = new Map_Object[20];
	protected static String mapName[] = new String[20];
//	protected screenWidth , screenHeight;
	static{
		mapName[1] = "Village1";
		mapName[2] = "V1_tent";
		mapName[3] = "V1_Pub";
		mapName[4] = "V1_inn";
		mapName[5] = "V1_shop";
		mapName[6] = "V1_headhouse";
		mapName[7] = "Forest1";
		for(int i=1 ; i<=7 ; i++){
			Map[i] = new Map_Object();
			try{
				readObjectsFromFile(i,"../map/"+mapName[i]);
			}	
			catch(FileNotFoundException e){
				System.out.println("No file!!");
			}
		}
		
	}
	public static void readObjectsFromFile(int MapNum,String filename)throws FileNotFoundException{
            File file = new File(filename);
            if(!file.exists()) 
                    throw new FileNotFoundException();
            try{
                    FileInputStream fileInputStream = new FileInputStream(file);
                    ObjectInputStream objInputStream = new ObjectInputStream(fileInputStream); 
                    while(fileInputStream.available() > 0) {
                            Map[MapNum] = ((Map_Object)objInputStream.readObject());
                    }
                    objInputStream.close();
            }
            catch(ClassNotFoundException e) { 
                    e.printStackTrace(); 
            } 
            catch(IOException e) { 
                    e.printStackTrace(); 
            }
            return;
    }
}
	