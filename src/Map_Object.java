/**
地圖物件(利用MapEditor進行編輯)
mapName : 地圖名稱
MAPSIZE : 地圖大小
x,y:地圖Panel在"Frame"上面的位置
characterX,characterY:腳色在地圖上的座標
mapInfo : 地圖"每一格"的狀態，定義如下
-1:障礙物
0:路
1~20:傳送點
21~25:事件觸發點
26~40:NPC
**/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
public class Map_Object extends JPanel{
	protected int MAPSIZE;
	protected int mapInfo[][];
	protected int x,y;
	protected int characterX,characterY;
	protected String mapName;
	protected String BGMName;
	protected JLabel BGLabel;
	protected JLabel mapLabel[];
	protected JLabel mapLabelHouse[] = new JLabel[10];
	protected JLabel mapLabelNPC[] = new JLabel[10];
	public Map_Object(){
		this.setLayout(null);
		this.setBackground(Color.black);
	}
	public void setMAPSIZE(int size){
		MAPSIZE = size;
		this.setSize(size,size);
	}
	public void createMapArray(int size){
		mapInfo = new int[size/50][size/50];
	}
	public void setCharacterX(int _x){
		characterX = _x;
	}
	public void setCharacterY(int _y){
		characterY = _y;
	}
	public void setScreenPos(){
		x=LosersAndWinner.screenWidth/2-characterX;
		y=LosersAndWinner.screenHeight/2-characterY;
	}
	public void setMapName(String name){
		mapName = name;
	}
	public void setBGMName(String name){
		BGMName = name;
	}
}