import java.util.Random;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Monster{
	final protected int HP;
	final protected int AGI;
	final protected int ATK;
	protected JLabel    MonsterLabel;
	protected ImageIcon BarIcon;
	
	protected int LeftHP;
	protected boolean alive;
	public JLabel BloodIcon;
	
	//get random monsters
	public static Monster GetMonster(int idx)
	{
		if(idx == 0)
			return new Monster_Pika();
		else if(idx == 1)
			return new Monster_Slime();
		else
		{
			System.out.println("out of range index"+idx);	
			return null;
		}
	}
	
	public Monster(int hp, int atk, int agi)
	{
		HP = hp;
		LeftHP = hp;
		ATK = atk;
		AGI = agi;
		BloodIcon = new JLabel();
		BloodIcon.setBackground(Color.red);
		BloodIcon.setOpaque(true);
	}
	
	public int  getHP()			{return LeftHP;}
	public int	getAGI()		{return AGI;}
	public void setHP(int hp){
		LeftHP = hp;
		System.out.println(LeftHP+"//"+HP);
		BloodIcon.setSize(100*LeftHP/HP,20);
		BloodIcon.repaint();
		
		
		if(LeftHP <= 0)
			Die();
	}
	
	public void Die()
	{
		FightingScreen.die(id);
	
	
	}
	
	public void Dead()		{alive = false;}
	public boolean isAlive(){return alive;}
	
}