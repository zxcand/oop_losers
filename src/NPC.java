/**
定義NPC的基本變數與method
變數:
sentenceCount : 紀錄目前講到第幾句話
totalSentence : 此NPC總共有幾句話
Method:
nextSentence(): 印出下一句話
talk(): 觸發與NPC的對話並印出對話框 腳色圖
end() : 結束對話並將控制權轉回主畫面
*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
public class NPC extends JLabel{
	protected String Name;
	protected String ID;
	protected int Number;
	protected MainScreen mainScreen;
	protected LosersAndWinner LAW;
	protected JLabel Dialog = new JLabel(AllIcons.dialogIcon);
	protected JTextArea text = new JTextArea();
	protected JLabel NPCName = new JLabel();
	protected ImageIcon Image;
	protected ImageIcon NPCMapIcon; 	
	protected JLabel NPCImage = new JLabel();
	protected NPCHandler npchandler = new NPCHandler(this);
	private String[] sentence;
	private int sentenceCount;
	private Thread walkThread;
	private int positionX,positionY;
	public NPC(MainScreen ms , LosersAndWinner jf,int NPCID){
		mainScreen = ms;
		LAW = jf;
		Number = NPCID;
		LoadNPCInfo(NPCID);
	}
	public void setPosition(int _x,int _y){
		positionX = _x;
		positionY = _y;
	}
	private void LoadNPCInfo(int NPCID){
		String[] Info;
		Info = XMLParser.NPCInfo(Integer.toString(NPCID));
		this.Name = Info[0];
		this.ID = Info[1];
		Image = new ImageIcon(Info[2]);
		NPCImage.setIcon(Image);
		NPCMapIcon = new ImageIcon(Info[3]);
		this.setIcon(NPCMapIcon);
	}
	protected boolean nextSentence(){
		if(sentenceCount<sentence.length){
			text.setText(sentence[sentenceCount]);
			sentenceCount++;
			return true;
		}
		else
			return false;
	}
	public void talk(){
		LAW.removeKeyListener(mainScreen.handler);
		LAW.addKeyListener(npchandler);
		/*Dialog*/
		LAW.add(Dialog,0);
		Dialog.setBounds(0,mainScreen.screenHeight-200,mainScreen.screenWidth,200);
		Dialog.setBackground(new Color(200, 230, 230));
		/*Text*/
		LAW.add(text,0);
		Font font=new Font(Font.DIALOG_INPUT,Font.BOLD, 30);
		text.setFont(font);
		text.setEditable(false);
		text.setBounds(0,mainScreen.screenHeight-135,mainScreen.screenWidth,135);
		text.setForeground(Color.white);
		text.setOpaque(false);
		/*NPCNameLabel*/
		LAW.add(NPCName,0);
		font=new Font(Font.DIALOG_INPUT,Font.ITALIC, 30);
		NPCName.setFont(font);
		NPCName.setText(Name);
		NPCName.setBounds(0,mainScreen.screenHeight-175,mainScreen.screenWidth,30);
		NPCName.setForeground(Color.red);
		/*NPCImage*/
		LAW.add(NPCImage,0);
		NPCImage.setBounds(mainScreen.screenWidth-250,mainScreen.screenHeight-400,250,400);
		
		LAW.repaint();
		sentence = XMLParser.NPCDialog(ID,EpisodeRecorder.episode[Number]);
		sentenceCount=0;
		nextSentence();
	}
	public void end(){
		LAW.removeKeyListener(npchandler);
		LAW.remove(Dialog);
		LAW.remove(text);
		LAW.remove(NPCName);
		LAW.remove(NPCImage);
		LAW.repaint();
		LAW.addKeyListener(mainScreen.handler);
		EpisodeRecorder.setEpisode(Number);
	}
	/*public void walking(){
		walkThread = new Thread(new Runnable(){
			public void run(){
				while(true){
					if(mainScreen.mapPanel.mapInfo[positionX][positionY+1]==0){
						
					else if(mainScreen.mapPanel.mapInfo[positionX][positionY-1]==0)
					else if(mainScreen.mapPanel.mapInfo[positionX-1][positionY]==0)
					else if(mainScreen.mapPanel.mapInfo[positionX+1][positionY]==0)
					
				)*/
		
}
class NPCHandler extends KeyAdapter{
	private NPC npc;
	public NPCHandler(NPC n){
		npc = n;
	}
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == e.VK_SPACE){
			if(!npc.nextSentence())
				npc.end();
		}
		
	}
}