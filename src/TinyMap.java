/**
小地圖資訊
*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class TinyMap extends JPanel{
	protected ImageIcon TinyMapIcon;
	
	protected JLabel MapLabel;
	protected JLabel MCPos;
	protected int mapSize;
	public TinyMap(String mapName,int _mapSize){
		this.setLayout(null);
		mapSize = _mapSize;
		MCPos = new JLabel(AllIcons.TinyMC);
		TinyMapIcon = new ImageIcon("../image/map/"+mapName+".png");
		TinyMapIcon.setImage(TinyMapIcon.getImage().getScaledInstance(200, 200, Image.SCALE_DEFAULT));
		MapLabel = new JLabel(TinyMapIcon);
		this.add(MapLabel, 0);
		this.add(MCPos,0);
		MapLabel.setBounds(0,0,200,200);
	}
	public void Move(int x,int y){
		MCPos.setBounds(x*200/(mapSize/50),y*200/(mapSize/50),200/(mapSize/50),200/(mapSize/50));
	}		
}