/* 所有的道具都會繼承weapon
method:
public void set_money(int _money)
public void set_name(String _name)
public void set_icon(ImageIcon _icon) 設置道具的圖示
public int get_money() 道具的價格
public String get_name() 
public ImageIcon get_icon() 得到道具的圖示
public void use(MC mc)
public void use(Monster monster)
*/

import java.awt.*;
import javax.swing.*;
import java.util.*;

public abstract class Weapon {
	private int money;
	private String name;
	private ImageIcon icon;
	private String description;
	private int type; //是屬於哪一種武器(1:手持武器 2.盾牌 3.頭盔 4.鎧甲)
	private boolean equiped;//是否有裝備在某一人物上，有=true 沒有=false
	private String equiper;//裝備在誰身上(名字)
	public void set_money(int _money) {
		money = _money;
	}
	public void set_name(String _name) {
		name = _name;
	}
	public void set_icon(ImageIcon _icon) {
		icon = _icon;
	}
	public void set_description(String _description) {
		description = _description;
	}
	public void set_type(int _type) {
		type = _type;
	}
	public void set_equiped(boolean _equiped, String _name) {
		equiped = _equiped;
		equiper = _name;
	}
	public int get_money() {
		return money;
	}
	public String get_name() {
		return name;
	}
	public ImageIcon get_icon() {
		return icon;
	}
	public String get_description() {
		return description;
	}
	public int get_type() {
		return type;
	}
	public boolean get_equiped() {
		return equiped;
	}
	public String get_equiper() {
		return equiper;
	}
	public abstract void equip(MC mc);
	public abstract void unequip(MC mc);
}
class Long_sword1 extends Weapon {
	public Long_sword1() {
		super.set_money(100);
		super.set_name("基礎長劍");
		//super.set_icon(long_sword1_icon);
		super.set_icon(AllIcons.long_sword1_icon);
		super.set_description("<html>手持武器<br>裝備後攻擊力增加10</html>");
		super.set_type(1);
		super.set_equiped(false,null);
	}
	public void equip(MC mc) {
		super.set_equiped(true,mc.getName());
		mc.setATT(mc.getATT()+10);
		
	}
	public void unequip(MC mc) {
		super.set_equiped(false,null);
		mc.setATT(mc.getATT()-10);
	}
}