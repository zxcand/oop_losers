import java.io.File; 
import java.io.IOException; 
import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory; 
import javax.xml.parsers.ParserConfigurationException; 
import org.w3c.dom.Document; 
import org.w3c.dom.Element; 
import org.w3c.dom.Node; 
import org.w3c.dom.NodeList; 
import org.xml.sax.SAXException; 
public class XMLParser { 
	private static DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance(); 
	//private static XMLParse parser = new XMLParser(); 
	public static Document parse(String filePath) { 
		Document document = null; 
		try { 
			DocumentBuilder builder = builderFactory.newDocumentBuilder(); 
			document = builder.parse(new File(filePath)); 
		} catch (ParserConfigurationException e) { 
			e.printStackTrace();  
		} catch (SAXException e) { 
			e.printStackTrace(); 
		} catch (IOException e) { 
			e.printStackTrace(); 
		} 
		return document; 
	}	 
	public static String[] NPCDialog(String NPCName,int episode) {
        Document document = XMLParser.parse("../xml/NPCDialog.xml"); 
        Element rootElement = document.getDocumentElement(); 
        NodeList nodeList = ((Element)rootElement.getElementsByTagName(NPCName).item(0)).getElementsByTagName("episode_"+Integer.toString(episode)); 
        if(nodeList != null) 
        { 
			Element element = (Element)nodeList.item(0);
			String num = element.getElementsByTagName("NUM").item(0).getTextContent();
			String[] dialog = new String[Integer.parseInt(num)];
			for(int i=0 ; i<Integer.parseInt(num) ; i++)
				dialog[i] = element.getElementsByTagName("SENTENCE").item(i).getTextContent();
			return dialog;
		}
		else
			return null;
	}
	public static String[] NPCInfo(String NPCID) {
        Document document = XMLParser.parse("../xml/NPCInfo.xml"); 
        Element rootElement = document.getDocumentElement(); 
        NodeList nodeList = rootElement.getElementsByTagName("INFO_"+NPCID);
		if(nodeList != null) 
        { 
			String[] Info = new String[4];
			Element element = (Element)nodeList.item(0);
			Info[0] = element.getElementsByTagName("NAME").item(0).getTextContent();
			Info[1] = element.getElementsByTagName("ID").item(0).getTextContent();
			Info[2] = element.getElementsByTagName("IMAGE").item(0).getTextContent();
			Info[3] = element.getElementsByTagName("ICON").item(0).getTextContent();
			return Info;
		}
		else
			return null;
	}
	public static String EventMessage(String eventID) {
        Document document = XMLParser.parse("../xml/Event.xml"); 
        Element rootElement = document.getDocumentElement(); 
        NodeList nodeList = rootElement.getElementsByTagName("EVENT_"+eventID);
		if(nodeList != null) 
        { 
			String message;
			Element element = (Element)nodeList.item(0);
			message = element.getElementsByTagName("MESSAGE").item(0).getTextContent();
			return message;
		}
		else
			return null;
	} 	 	
 } 