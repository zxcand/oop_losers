/**
儲存所有主角(三位)圖片物件，依照陣列index來指定
ex MC[1][2][3]
第一維為角色編號
第二維為上(0)下(1)左(2)右(3)
第三維為三種分解動作代號
*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
public class  characterIcon{
	protected ImageIcon MC[][][] = new ImageIcon[3][4][4];
	//protected ImageIcon MC2[][] = new ImageIcon[4][4];
	//protected ImageIcon MC3[][] = new ImageIcon[4][4];
	protected int step;
	public void CountStep(){
		step++;
		if(step>=12)
			step=0;
	}
	public characterIcon(){
		MC[0][0][0] = new ImageIcon("../image/Character/MC1_U1.png");
		MC[0][0][1] = new ImageIcon("../image/Character/MC1_U2.png");
		MC[0][0][2] = new ImageIcon("../image/Character/MC1_U3.png");
		MC[0][0][3] = new ImageIcon("../image/Character/MC1_U2.png");
		MC[0][1][0] = new ImageIcon("../image/Character/MC1_D1.png");
		MC[0][1][1] = new ImageIcon("../image/Character/MC1_D2.png");
		MC[0][1][2] = new ImageIcon("../image/Character/MC1_D3.png");
		MC[0][1][3] = new ImageIcon("../image/Character/MC1_D2.png");
		MC[0][2][0] = new ImageIcon("../image/Character/MC1_L1.png");
		MC[0][2][1] = new ImageIcon("../image/Character/MC1_L2.png");
		MC[0][2][2] = new ImageIcon("../image/Character/MC1_L3.png");
		MC[0][2][3] = new ImageIcon("../image/Character/MC1_L2.png");
		MC[0][3][0] = new ImageIcon("../image/Character/MC1_R1.png");
		MC[0][3][1] = new ImageIcon("../image/Character/MC1_R2.png");
		MC[0][3][2] = new ImageIcon("../image/Character/MC1_R3.png");
		MC[0][3][3] = new ImageIcon("../image/Character/MC1_R2.png");
		
		MC[1][0][0] = new ImageIcon("../image/Character/MC2_U1.png");
		MC[1][0][1] = new ImageIcon("../image/Character/MC2_U2.png");
		MC[1][0][2] = new ImageIcon("../image/Character/MC2_U3.png");
		MC[1][0][3] = new ImageIcon("../image/Character/MC2_U2.png");
		MC[1][1][0] = new ImageIcon("../image/Character/MC2_D1.png");
		MC[1][1][1] = new ImageIcon("../image/Character/MC2_D2.png");
		MC[1][1][2] = new ImageIcon("../image/Character/MC2_D3.png");
		MC[1][1][3] = new ImageIcon("../image/Character/MC2_D2.png");
		MC[1][2][0] = new ImageIcon("../image/Character/MC2_L1.png");
		MC[1][2][1] = new ImageIcon("../image/Character/MC2_L2.png");
		MC[1][2][2] = new ImageIcon("../image/Character/MC2_L3.png");
		MC[1][2][3] = new ImageIcon("../image/Character/MC2_L2.png");
		MC[1][3][0] = new ImageIcon("../image/Character/MC2_R1.png");
		MC[1][3][1] = new ImageIcon("../image/Character/MC2_R2.png");
		MC[1][3][2] = new ImageIcon("../image/Character/MC2_R3.png");
		MC[1][3][3] = new ImageIcon("../image/Character/MC2_R2.png");
	
		MC[2][0][0] = new ImageIcon("../image/Character/MC3_U1.png");
		MC[2][0][1] = new ImageIcon("../image/Character/MC3_U2.png");
		MC[2][0][2] = new ImageIcon("../image/Character/MC3_U3.png");
		MC[2][0][3] = new ImageIcon("../image/Character/MC3_U2.png");
		MC[2][1][0] = new ImageIcon("../image/Character/MC3_D1.png");
		MC[2][1][1] = new ImageIcon("../image/Character/MC3_D2.png");
		MC[2][1][2] = new ImageIcon("../image/Character/MC3_D3.png");
		MC[2][1][3] = new ImageIcon("../image/Character/MC3_D2.png");
		MC[2][2][0] = new ImageIcon("../image/Character/MC3_L1.png");
		MC[2][2][1] = new ImageIcon("../image/Character/MC3_L2.png");
		MC[2][2][2] = new ImageIcon("../image/Character/MC3_L3.png");
		MC[2][2][3] = new ImageIcon("../image/Character/MC3_L2.png");
		MC[2][3][0] = new ImageIcon("../image/Character/MC3_R1.png");
		MC[2][3][1] = new ImageIcon("../image/Character/MC3_R2.png");
		MC[2][3][2] = new ImageIcon("../image/Character/MC3_R3.png");
		MC[2][3][3] = new ImageIcon("../image/Character/MC3_R2.png");
	}
}