import java.util.*;
import java.awt.*;  
import java.awt.event.*;  
import javax.swing.*;  
import java.awt.Toolkit; 


public class login extends JPanel{
	private LosersAndWinner jf;
	private JPanel jp;
	private JLabel[] lbl;
	private JLabel BGLabel;
	private int ptr;
	private int state;
	private LoginListener loginHandler = new LoginListener();
	private login loginself;
	
	public login(LosersAndWinner caller)
	{
		try{
			BGMPlayer.playBGM("LoginBGM.wav");
		}catch(Throwable t){}
	//	jf = new JFrame();
		jf = caller;
		jf.add(this);
		loginself=this;
		this.setSize(LosersAndWinner.screenWidth,LosersAndWinner.screenHeight);
		this.setLayout(null);
		//bg = new ImageIcon("..\\image\\bg.jpg").getImage();
		AllIcons.LogInbackGround.setImage(AllIcons.LogInbackGround.getImage().getScaledInstance(jf.screenWidth, jf.screenHeight, Image.SCALE_DEFAULT));
		BGLabel = new JLabel(AllIcons.LogInbackGround);
		this.add(BGLabel);
		BGLabel.setBounds(0,0,jf.screenWidth,jf.screenHeight);
		lbl = new JLabel[4];
		lbl[0] = new JLabel("Press any key...");
		add(lbl[0],0);
		lbl[0].setFont(new Font("Comic Sans MS", Font.PLAIN, 50));
		lbl[0].setForeground(Color.white);
		lbl[0].setHorizontalAlignment(JLabel.CENTER);
		lbl[0].setBounds(0,jf.screenHeight/2-30,jf.screenWidth,50);
		lbl[1] = new JLabel();
		add(lbl[1],0);
		lbl[1].setFont(new Font("Comic Sans MS", Font.PLAIN, 50));
		lbl[1].setForeground(Color.blue);
		lbl[1].setHorizontalAlignment(JLabel.CENTER);
		lbl[1].setBounds(0,jf.screenHeight/2+20,jf.screenWidth,50);
		lbl[2] = new JLabel();
		add(lbl[2],0);
		lbl[2].setFont(new Font("Comic Sans MS", Font.PLAIN, 50));
		lbl[2].setForeground(Color.blue);
		lbl[2].setHorizontalAlignment(JLabel.CENTER);
		lbl[2].setBounds(0,jf.screenHeight/2+70,jf.screenWidth,50);
		lbl[3] = new JLabel();
		add(lbl[3],0);
		lbl[3].setFont(new Font("Comic Sans MS", Font.PLAIN, 50));
		lbl[3].setForeground(Color.blue);
		lbl[3].setHorizontalAlignment(JLabel.CENTER);
		lbl[3].setBounds(0,jf.screenHeight/2+120,jf.screenWidth,50);
		repaint();
		jf.repaint();
		ptr = 0;
		state = 0;
		//lbl[0].setBounds(400,300,200,200);
		//setLayout(new GridLayout(4,1));
		jf.addKeyListener(loginHandler);
		//setFocusable(true);
		//jf.setVisible(true);
	}
	
	/*
	@Override
	public void paint(Graphics g)
	{
		System.out.println("painting");
		
		g.drawImage(bg, 0, 0, null);
		super.paint(g);
	}
	*/
	/*public void paintComponent(Graphics g) {
		System.out.println("painting componet");
		g.drawImage(bg, 0, 0, null);
	}*/
	class LoginListener extends KeyAdapter
	{
		public void keyPressed(KeyEvent e) 
		{
			//bg = new ImageIcon("..\\image\\bg2.jpg").getImage();
			if(e.getKeyCode() == KeyEvent.VK_UP && state == 1)
			{
				lbl[ptr].setForeground(Color.blue);
				ptr += (lbl.length-1);
				ptr %= lbl.length;
				lbl[ptr].setForeground(Color.white);
			}
			else if(e.getKeyCode() == KeyEvent.VK_DOWN && state == 1)
			{
				lbl[ptr].setForeground(Color.blue);
				ptr++;
				ptr = ptr % lbl.length;
				lbl[ptr].setForeground(Color.white);
			}
			
			else if(ptr == 0 && state == 1 && (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE))
			{
				
				//try
				//{
					BGMPlayer.BGMclip.stop();
					//jf.getContentPane().removeAll();
					jf.removeKeyListener(loginHandler);
					jf.remove(loginself);
					jf.add(new MainScreen(jf));
				//	jf.revalidate();
					jf.repaint();
					/*
					new MainScreen();
					JPanel ms = new MainScreen();
					ms.showscreen();
					*/
				//}
				//catch(Exception ee)
				//{
				//	System.out.println(ee);
				//	System.out.println("err");
				//}
			}
			
			else if(ptr == 3 && state == 1 && (e.getKeyCode() == KeyEvent.VK_ENTER|| e.getKeyCode() == KeyEvent.VK_SPACE))
			{
				System.exit(0);			
			}
			else
			{
				//System.out.println("pressed else");
				state = 1;
				lbl[0].setText("New Game");
				lbl[1].setText("Load Game");
				lbl[2].setText("Option");
				lbl[3].setText("Exit");
				repaint();		
			}
		} 
	}
	/*public static void music(){
		AudioPlayer MGP = AudioPlayer.player;
		AudioStream BGM;
		AudioData MD;
		ContinuousAudioDataStream loop = null;
		try{
			BGM = new AudioStream(new FileInputStream("LoginBGM.wav"));
			MD = BGM.getData();
			loop = new ContinuousAudioDataStream(MD);
		}
		catch(IOException error){System.out.println(error);}
		MGP.start(loop);
		
	}*/
	
			
}


