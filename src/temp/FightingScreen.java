/*	需要傳入MC的list
*/

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class FightingScreen extends JPanel
{	
	public static JFrame mainJF;

	private static final int[][] mc_pos = {{LosersAndWinner.screenWidth-400,LosersAndWinner.screenHeight-500},{LosersAndWinner.screenWidth-500,LosersAndWinner.screenHeight-400},{LosersAndWinner.screenWidth-600,LosersAndWinner.screenHeight-300}};
	private static final int[][] monst_pos = {{125,325},{225,225},{325,125}};

	private JLabel BGLabel;
	private characterIcon MCIcon = new characterIcon();
	private JLabel[] MCLabel = new JLabel[3];
	private Fighting_ProgBar pb;
	private LinkedList OurTeam;
	private KeyAdapter mapHandler;
	private int Monst_level;
	private int turn;
	private static JLabel mc_arrow = new JLabel();
	static{
		mc_arrow.setIcon(AllIcons.Arrow);	
	}
	
	//paras for progressbar
	public static final int bar_pos_x = LosersAndWinner.screenWidth-530;
	public static final int bar_pos_y = 60;
	public static final int bar_width  = 500;
	public static final int bar_height = 100;
	
	public FightingScreen(JFrame jf,KeyAdapter _mapHandler)
	{
		this(jf, 1);
		mapHandler=_mapHandler;
		Fighting_ProgBar.RESUME();
		
	}
	
	public FightingScreen(JFrame jf, int level)//int situation
	{		
		/*mainJF = caller;
		screenWidth=mainJF.screenWidth;
		screenHeight=mainJF.screenHeight;
		FightBG.setImage(FightBG.getImage().getScaledInstance(mainJF.screenWidth, mainJF.screenHeight, Image.SCALE_DEFAULT));
		*/
		turn = 0;	
		this.setOpaque(true);
		this.setLayout(null);
		this.mainJF = jf;
		this.setBounds(0, 0, LosersAndWinner.screenWidth, LosersAndWinner.screenHeight);
		this.Monst_level = level;
		
		Image ii;
		if(Monst_level == 1)
			 ii= AllIcons.FightBG.getImage().getScaledInstance(LosersAndWinner.screenWidth, LosersAndWinner.screenHeight, Image.SCALE_SMOOTH);
		else
			ii = AllIcons.FightBG2.getImage().getScaledInstance(LosersAndWinner.screenWidth, LosersAndWinner.screenHeight, Image.SCALE_SMOOTH);
		BGLabel = new JLabel(new ImageIcon(ii));
		this.add(BGLabel,0);
		BGLabel.setBounds(0,0, LosersAndWinner.screenWidth, LosersAndWinner.screenHeight);//LosersAndWinner.screenWidth, LosersAndWinner.screenHeight);
		
		for(int i=0; i<3; i++)
		{
			MCLabel[i] = new JLabel();
			if(i == 0)
				MCLabel[i].setIcon(new ImageIcon("../image/fighting/a1.png"));//MCIcon.MC[i][2][1]);
			else if(i==1)
				MCLabel[i].setIcon(new ImageIcon("../image/fighting/a2.png"));
			else
				MCLabel[i].setIcon(new ImageIcon("../image/fighting/a3.png"));			
			
			this.add(MCLabel[i],0);
			MCLabel[i].setBounds(mc_pos[i][0],mc_pos[i][1],100,100);
			
			//this.moveToFront(MCLabel[i]);
			
			//add char info to panel
			//this.add(Fighting_CharacterInfo(MC));
		}
		AddMonst();
		AddProgBar();		
	}
	
	public void AddMonst()
	{
		JLabel pika = new JLabel();
		pika.setIcon(new ImageIcon("..\\image\\pikaman.png"));
		JLabel slime = new JLabel();
		slime.setIcon(new ImageIcon("..\\image\\slime.png"));
		
		pika.setBounds(monst_pos[0][0],monst_pos[0][1],100,100);
		this.add(pika,0);
		
		slime.setBounds(monst_pos[1][0],monst_pos[1][1],100,100);
		this.add(slime,0);
	}
		
	public void AddProgBar()
	{
		pb = new Fighting_ProgBar(this);
		pb.setBounds(bar_pos_x,bar_pos_y,bar_width,bar_height);
		this.add(pb,0);
		
		Thread t = new Thread(pb);
		t.start();
	}
	
	public void Apply_skill()//(MC now)
	{
		//showing the menu of skill that can be appllied
		//MC mc = now;

		//Skill sk
		myInfo info = new myInfo(0,-1);
		int action;
		int target;
		
		JLabel menu = new AttackMenu(info);
		menu.setBounds(LosersAndWinner.screenWidth-350,LosersAndWinner.screenHeight-350,250,250);
		add(menu,0);
		repaint();
		
		//(1) Choosing skill(assign value to var. sk)
		while(true)
		{
			action = info.action;
			
			//whose turn(?)
			
			mc_arrow.setBounds(mc_pos[turn][0]-15+50, mc_pos[turn][1]-50, 30, 50);
			add(mc_arrow,0);
			repaint();	
			
			if(action == 0)
			{
				try{
					Thread.sleep(500);
				}
				catch(InterruptedException ie){}
				
				continue;
			}
			
			turn = (turn+1)%3;
			if(action== 1)//攻
			{
				System.out.println("Normal Attack");
				//mc.NormalATK()
				remove(mc_arrow);
				repaint();
				break;
			}
			else if(action == 2)//術
			{
				System.out.println("Special Attack");
				//mc.SpecialATK()
				remove(mc_arrow);
				repaint();
				break;
			}
			else if(action == 3)//防
			{
				System.out.println("Defense");
				//mc.DEF()
				remove(mc_arrow);
				repaint();
				break;
			}
			else if(action == 4)//物
			{
				System.out.println("Using Item");
				MainScreen.bag.show_bag();
				while(true)
				{	
					if(Bag.selected == false && Bag.ReturnFromESC==false)
					{
						try{
						Thread.sleep(20);
						}
						catch(InterruptedException ie){}
					}
					else
						break;
				}

				remove(mc_arrow);
				repaint();
				
				if(Bag.ReturnFromESC==true)
				{
					info = new myInfo(0,-1);
					menu = new AttackMenu(info);
					menu.setBounds(LosersAndWinner.screenWidth-350,LosersAndWinner.screenHeight-350,250,250);
					add(menu,0);
					repaint();
					continue;
				}
				
				break;
			}
			else if(action == 5)//撤
			{
				System.out.println("Quit");
				//mc.quit() //based on some probability
				
				remove(mc_arrow);
				repaint();
				break;
			}
		}
		
		//[TODO] esc to jump back(goto?)
		remove(menu);
		repaint();
				
		//(2) Choosing target
		if(action==5)
		{
			BGMBattleclip.stop();
			Fighting_ProgBar.END();
			mainJF.remove(this);
			mainJF.repaint();
			mainJF.addKeyListener(mapHandler);
			
		}
		else if(action!=3 && action!=4 && action!=5)
		{
			JLabel arrow = new TargetArrow(info);
			arrow.setBounds(monst_pos[0][0]-15+50, monst_pos[0][1]-50, 30, 50);
			add(arrow,0);
			repaint();		
			
			while(true)
			{
				target = info.target;
				if(target  == -1)
				{
					try{
						Thread.sleep(500);
					}
					catch(InterruptedException ie){}
				}
				
				if(target != -1)
				{
					System.out.println("Attacking moster#"+target);
					//applySkill
					break;
				}
			}
			remove(arrow);
			repaint();
		}
		
		Fighting_ProgBar.start();
	}
	
	class myInfo
	{
		public int action;
		public int target;
		
		public myInfo(int a, int t)
		{
			action = a;
			target = t;	
		}
	}
	
	
	
	class AttackMenu extends JLabel
	{
		int state;
		myInfo info;
		AttackMenuListener temp_listener;
		
		public AttackMenu(myInfo i)
		{
			info = i;
			state = i.action;
			
			setIcon(AllIcons.menu0);
			
			temp_listener = new AttackMenuListener();
			FightingScreen.mainJF.addKeyListener(temp_listener);
		}

		class AttackMenuListener extends KeyAdapter
		{
			public void keyPressed(KeyEvent e) 
			{
				if(e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE)
				{
					info.action = state;
					FightingScreen.mainJF.removeKeyListener(temp_listener);
				}
				else if(e.getKeyCode() == KeyEvent.VK_UP)
				{
					state = 1;
					setIcon(AllIcons.menu1);
				}
				else if(e.getKeyCode() == KeyEvent.VK_LEFT)
				{	
					if(state == 5)
					{
						state = 4;
						setIcon(AllIcons.menu4);
					}
					else
					{
						state = 2;
						setIcon(AllIcons.menu2);
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_RIGHT)
				{
					if(state == 4)
					{
						state = 5;
						setIcon(AllIcons.menu5);
					}
					else
					{
						state = 3;
						setIcon(AllIcons.menu3);
					}
				}
				else if(e.getKeyCode() == KeyEvent.VK_DOWN)
				{
					if(state == 3)
					{
						state = 5;
						setIcon(AllIcons.menu5);
					}
					else
					{	
						state= 4;
						setIcon(AllIcons.menu4);
					}
				}
				else
					System.out.println("err key input");
			}
		}
	}
	
	
	
	class TargetArrow extends JLabel
	{
		int selected;
		myInfo info;
		TargetListener temp_listener;
		
		public TargetArrow(myInfo i)
		{
			info = i;
			selected = 0;//i.target;
			setIcon(AllIcons.Arrow);
			temp_listener = new TargetListener();
			FightingScreen.mainJF.addKeyListener(temp_listener);
		}
				
		
		class TargetListener extends KeyAdapter
		{
			public void keyPressed(KeyEvent e) 
			{
				if(e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_SPACE)
				{
					info.target = selected;;
					FightingScreen.mainJF.removeKeyListener(temp_listener);
				}
				else
				{
					selected = (selected+1)%2;
					setBounds(FightingScreen.monst_pos[selected][0]-15+50, FightingScreen.monst_pos[selected][1]-50, 30, 50);
				}
			}				
		}
	
	}
}